import editdistance
import re

from autocorrect import Speller

spell = Speller(lang='en')

from kvp_app.regex_kvp_utils import *

lookup_list = provider_lable + check_no_lable + patient_name_lable +\
              pat_member_id + patient_acc_no + Claim_ID_lable + dos_lable +\
              Provider_name_lable + payee_id_lable


def page_text_correction(text):
    sentence_list = set(re.split('\n|[,:#]', text))
    for sentence in sentence_list:
        sentence = sentence.strip()
        sentence = spell(sentence)
        # fitered_lookup_list = []
        # for i in range(sentence_len - 3, sentence_len + 4):
        #     _list = lookup_list_map.get(i, [])
        #     fitered_lookup_list = fitered_lookup_list + _list
        #
        for keyword in lookup_list:
            if len(keyword) > 5:
                if editdistance.eval(sentence, keyword) <= 1:
                    text = text.replace(sentence, keyword)
                    break
    return text


header_dict = {'Provider_NPI': provider_lable, 'Check_No': check_no_lable, 'Patient_Name': patient_name_lable,
              'Processed_Date': Processed_Date_lable, 'Payee_ID': payee_id_lable, 'Patient_Mem_ID': pat_member_id,
              'Patient_Acct_No': patient_acc_no, 'Group_No': group_lable, 'Claim_Id':Claim_ID_lable,
              'Check_Amount': chk_amount_lable, 'See_Remark': see_remark, 'GRP_RC_AMT': GRP_RC_AMT_lable,
              'Provider_Name': Provider_name_lable, 'Group_Name': group_name_lable, 'Claim_Status': claim_status_lable,
              'Not_Payable': not_payable_lable, 'Co_Insurance': co_insurance_lable,
              'Deductible': deductible_lable, 'CoPay': co_pay_lable, 'Page_no': ['Page_no'],
              'Check_Date': check_date_lable, 'Tax_ID': tax_id_lable}


def merge_forms_and_regex(textract_forms, regex_response):
    new_t_f_list = []
    for i in textract_forms:
        for k, v in i.items():
            for a, b in header_dict.items():
                for header in b:
                    header = header.lower().strip()
                    k = spell(k.lower().strip())
                    if editdistance.eval(header, k) <= 1:
                        n_d = {'label': a, 'extraction': v}
                        new_t_f_list.append(n_d)
    new_t_f_list_1 = [dict(t) for t in {tuple(d.items()) for d in new_t_f_list}]
    regex_label = []
    t_f_label = []
    for j in regex_response:
        regex_label.append(j['label'])
    for i in new_t_f_list_1:
        t_f_label.append(i['label'])
    new_l = list(sorted(set(regex_label) - set(t_f_label)))
    for i in new_l:
        for v in regex_response:
            if v['label'] == i:
                if v['extraction'] != None:
                    new_t_f_list_1.append(v)
    return new_t_f_list_1