import pytesseract
from pdf2image import convert_from_bytes
import re
from PIL import Image
import numpy as np
import cv2
import editdistance

from kvp_app.regex_kvp_utils import format_lable
from kvp_extraction import settings

path = settings.MEDIA_ROOT
import matplotlib.pyplot as plt


def pdf_format(file_name):
    pages3_1 = convert_from_bytes(open(file_name,'rb').read())
    text = ''
    pages3 = pages3_1[:3]
    for page in pages3:
        page.save(path + '/_page.jpg', 'jpeg')
        page_name = path + '/_page.jpg'
        try:
            degree = pytesseract.image_to_osd(Image.open(page_name))
            if "Rotate: 90" in degree:
                src = cv2.imread(page_name)
                img = cv2.rotate(src, cv2.cv2.ROTATE_90_CLOCKWISE)

                plt.imsave(path + "/Py_Rotated_image.png", img)
                rotate_image = path + "/Py_Rotated_image.png"
            elif "Rotate: 270" in degree:
                src = cv2.imread(page_name)
                img = cv2.rotate(src, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)

                plt.imsave(path + "/Py_Rotated_image.png", img)
                rotate_image = path + "/Py_Rotated_image.png"
            else:
                rotate_image = page_name
        except:
            rotate_image = page_name

        config = '--psm 6'
        img = np.array(Image.open(rotate_image))
        text += pytesseract.image_to_string(img, lang='eng', config=config)
    page_format = find_page_format(text)
    return page_format, pages3_1


def find_page_format(text):
    page_format = ''
    for i in format_lable:
        found1 = re.search(format_lable[i][0], text, re.IGNORECASE)
        found2 = re.search(format_lable[i][1], text, re.IGNORECASE)

        if len(format_lable[i]) == 3:
            found3 = re.search(format_lable[i][2], text, re.IGNORECASE)
            if found1 and found2:
                page_format = i
            elif found2 and found3:
                page_format = i
        if found1 and found2:
            page_format = i

    return page_format


def dark_img_procesing(image):
    img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    img = cv2.multiply(img, 3)
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.erode(img, kernel, iterations=1)

    return img


def table_dark_img_procesing(image):
    img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    img = cv2.multiply(img, 0.5)
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.erode(img, kernel, iterations=1)

    return img


def text_correction(text):
    str_1 = re.findall(r"[\w']+", text)
    for i in str_1:
        if editdistance.eval('Patient', i) <= 3:
            text = text.replace(i, 'Patient')
        if editdistance.eval('Provider', i) <= 3:
            text = text.replace(i, 'Provider')
        if editdistance.eval('Claim', i) <= 1:
            text = text.replace(i, 'Claim')
        if editdistance.eval('Account', i) <= 1:
            text = text.replace(i, 'Account')
        if editdistance.eval('Amount', i) <= 1:
            text = text.replace(i, 'Amount')
        if editdistance.eval('Name', i) <= 1:
            text = text.replace(i, 'Name')
        if editdistance.eval('Check', i) <= 1:
            text = text.replace(i, 'Check')
        if editdistance.eval('Payee', i) <= 1:
            text = text.replace(i, 'Payee')
        if editdistance.eval('NPI', i) <= 1:
            text = text.replace(i, 'NPI')
        if editdistance.eval('Provider NPI', i) <= 3:
            text = text.replace(i, 'Provider NPI')
    return text


# To Multiple File Run
# def home(request):
#     """
#     :param request:
#     :return:
#     """
#     # try:
#     table_json = []
#     client_name = ''
#     page_format = ''
#     final_dfs = []
#     text = ''
#     config = '--psm 6 -c tessedit_char_blacklist=`~!@%^&*()£_-“+={[}}|;‘©,<>?”'
#     csv_file = 'output.csv'
#     html_df = []
#     list_of_new_dfs = []
#     directory = path + '/All_files'
#     for filename in os.listdir(directory):
#         if filename.endswith(".pdf"):
#             print(filename)
#             files = path + '/All_files/' + filename
#             # patient_name = ['Patient Name']
#             # claim_id = 'Claim id'
#             # client_lable = ['aetna', 'cigna', 'unitedhealthcare']
#             # SCALE = 9
#             # try:
#             if os.path.exists(path + '/output.csv'):
#                 os.remove(path + '/output.csv')
#             # if request.method == 'POST' and request.FILES['files']:
#             #     myfile = request.FILES['files']
#             #     fs = FileSystemStorage()
#             #     filename = fs.save(myfile.name, myfile)
#             #     files = path + '/' + filename
#             #     file_type = filetype.guess(files)
#                 image_or_file = ''
#             pat_info_list = []
#             # if file_type.extension == 'pdf':
#             # with open(files, 'rb') as pdf:
#             pages3 = convert_from_bytes(open(files, 'rb').read())
#             # first_two_page = pages3[:2]
#             # first_page_text = ''
#             # for first_page in first_two_page:
#             #     first_page.save(path + '/first_page.jpg', 'jpeg')
#             #     first_page_name = path + '/first_page.jpg'
#             #     img = np.array(Image.open(first_page_name))
#             #     first_page_text += pytesseract.image_to_string(img, lang='eng', config='--psm 3')
#             # client_name = ''
#             # for i in client_lable:
#             #     found = re.search(i, first_page_text.lower())
#             #     if found:
#             #         client_name = found.group()
#             # # model_name = ''
#             # # if client_name == 'aetna':
#             # #     model_name = 'aetna'
#             # # elif client_name == 'cigna':
#             # #     model_name = 'cigna'
#             # # elif client_name == 'unitedhealthcare':
#             # #     model_name = 'unitedhealthcare'
#             page_format = pdf_format(files)
#             # print('page_format=============', page_format)
#             for page in pages3:
#                 page.save(path + '/converted.jpg', 'jpeg')
#                 documentName = path + '/converted.jpg'
#
#                 # dict_1 = text_extract_v1(documentName, model_name)
#                 try:
#                     degree = pytesseract.image_to_osd(Image.open(documentName))
#                     if "Rotate: 90" in degree:
#                         SCALE = 20
#                         src = cv2.imread(documentName)
#                         img = cv2.rotate(src, cv2.cv2.ROTATE_90_CLOCKWISE)
#                         plt.imsave(path + "/Py_Rotated_image.png", img)
#                         rotate_image = path + "/Py_Rotated_image.png"
#                     elif "Rotate: 270" in degree:
#                         src = cv2.imread(documentName)
#                         img = cv2.rotate(src, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)
#                         plt.imsave(path + "/Py_Rotated_image.png", img)
#                         rotate_image = path + "/Py_Rotated_image.png"
#                     else:
#                         rotate_image = documentName
#                 except Exception as e:
#                     print(e)
#                     rotate_image = documentName
#                 input_noteshrink = argparse.Namespace(basename='page',
#                                                       filenames=[rotate_image],
#                                                       global_palette=False, num_colors=8,
#                                                       postprocess_cmd=None, quiet=False,
#                                                       sample_fraction=0.05, sat_threshold=0.2,
#                                                       saturate=True, sort_numerically=True,
#                                                       value_threshold=0.25, white_bg=False)
#
#                 output_filename = notescan_main(input_noteshrink)
#                 try:
#                     # img_for_table = table_dark_img_procesing(rotate_image)
#                     # plt.imsave(path + "/table_input_image.png", img_for_table)
#                     # img_for_table1 = path + "/table_input_image.png"
#                     if page_format == 'Aetna_2':
#                         table__, pat_infoo = atena_2_table_textract(rotate_image)
#                         # pat_info_list.append(pat_infoo)
#                     elif page_format == 'Aetna_3':
#                         table__, pat_infoo = atena_3_table_textract(rotate_image)
#                         pat_info_list.append(pat_infoo)
#                     elif page_format == 'Aetna_1':
#                         aetna_1_table_textract(rotate_image)
#                     elif page_format == 'Aetna_4':
#                         aetna_4_table_textract(rotate_image)
#                     elif page_format == 'CIGNA_3':
#                         cigna_3_table_textract(rotate_image)
#
#                     elif page_format == 'CIGNA_2':
#                         cigna_2_table_textract(rotate_image)
#                     elif page_format == 'CIGNA_4':
#                         cigna_4_table_textract(rotate_image)
#                     elif page_format == 'UHC_1':
#                         uhc_1_table_textract(rotate_image)
#                     elif page_format == 'UHC_5':
#                         uhc_5_table_textract(rotate_image)
#                     else:
#                         table_textract(rotate_image)
#                 except Exception as e:
#                     print(e)
#                 # if page_format == 'Aetna_3':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'Aetna_2':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'Aetna_5':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'CIGNA_2':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'CIGNA_4':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'BCBS_1':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'UHC_3':
#                 #     img = output_filename
#                 # else:
#                 #     # ######## img = np.array(Image.open(output_filename))
#                 #     img = dark_img_procesing(output_filename)
#                 # text += pytesseract.image_to_string(img, lang='eng', config=config)
#                 try:
#                     text += textrect(output_filename)
#                 except Exception as e:
#                     print(e)
#
#                 # crop_image_list = tabnet_res(output_filename)
#                 df_lists = []
#                 # for crop_img in crop_image_list:
#                 #     table_df = extract_cell_images_from_table(crop_img, SCALE)
#                 #     df = df_structure(table_df)
#                 #     df_lists.append(df)
#                 final_dfs.append(df_lists)
#
#                     # if dict_1:
#                     #     # final_list.append([{'label': 'Page No: ' + str(pages3.index(page) + 1),
#                     #     #                     'prob': '',
#                     #     #                     'extraction': ''}])
#                     #     final_list.append(dict_1['preds'])
#                     # else:
#                     #     final_list.append({})
#             # result_kvp = list(itertools.chain.from_iterable(final_list))
#             # text = text_correction(text)
#             text = text.replace(':\n', ': ')
#             result_kvp, patient_name_l_1, claim_id_l_1, patient_acct_no_l_1 = find_formate(text, page_format)
#
#             pat_info_table_index = []
#             # GEt Table Sequence By Format
#             if page_format == 'Aetna_3':
#                 pat_info1 = [item for sublist in pat_info_list for item in sublist]
#                 all_data = patient_name_l_1 + claim_id_l_1 + patient_acct_no_l_1
#                 pat_info2 = []
#                 for i in pat_info1:
#                     i = i.replace('Patient Name:', ' ')
#                     i = i.replace('Patient Account:', ' ')
#                     i = i.replace('Patient Account:', ' ')
#                     i = i.replace('Patient Accounta', ' ')
#                     m = i.split()
#                     for j in m:
#                         for k in all_data:
#                             if editdistance.eval(k, j) <= 3:
#                                 i = i.replace(j, k)
#                     pat_info2.append(i)
#
#                 index_dict = {}
#                 count = 0
#                 for i, j, k in zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, fillvalue=""):
#                     index_dict[count] = [i, j, k]
#                     count += 1
#                 for i in pat_info2:
#                     for j in index_dict:
#                         for k in index_dict[j]:
#                             if k in i:
#                                 if j not in pat_info_table_index:
#                                     pat_info_table_index.append(j)
#
#             for i in result_kvp:
#                 if i['extraction'] is not None:
#                     if i['extraction'] == '':
#                         val = get_key_info(text, i['label'])
#                         if val:
#                             i['extraction'] = val[0] or i['extraction']
#                     if i['label'] == 'Processed_Date' or i['label'] == 'Date':
#                         date = get_date_info(i['extraction'])
#                         if date:
#                             i['extraction'] = date[0] or i['extraction']
#                             i['extraction'] = re.sub(r'^.*?(Date|:|DATE:|Date:|DATE)', '', i['extraction'])
#                     if i['label'] == 'Check_No':
#                         check_no = get_numeric_info(i['extraction'])
#                         if check_no:
#                             i['extraction'] = check_no[0] or i['extraction']
#                             i['extraction'] = get_trim_info(i['extraction'])[0]
#                     if i['label'] == 'Check_Amount':
#                         check_amt = get_float_info(i['extraction'])
#                         if check_amt:
#                             i['extraction'] = check_amt[0] or i['extraction']
#                     if i['label'] == 'Patient_Mem_ID':
#                         patient_mem_id = get_float_info(i['extraction'])
#                         if patient_mem_id:
#                             i['extraction'] = patient_mem_id[0] or i['extraction']
#                             i['extraction'] = get_trim_info(i['extraction'])[0]
#                     if i['label'] == 'Claim_Id':
#                         if page_format == 'UHC_1':
#                             Claim_Id = get_numeric_info_uhc_1(i['extraction'])
#                             i['extraction'] = Claim_Id[0] or i['extraction']
#                             i['extraction'] = i['extraction'].replace('SACKETT', '')
#                             i['extraction'] = i['extraction'].replace('SACKETT', '')
#                         else:
#                             Claim_Id = get_numeric_info(i['extraction'])
#                             if Claim_Id:
#                                 i['extraction'] = Claim_Id[0] or i['extraction']
#                                 i['extraction'] = get_trim_info(i['extraction'])[0]
#                     if i['label'] == 'Patient_Name':
#                         Patient_Name = re.sub('\W+', ' ', i['extraction'])
#                         Patient_Name = re.sub(r'^.*?(Name|name|NAME|PATIENT|Patient)', '', Patient_Name)
#                         Patient_Name = Patient_Name.replace('PATIENT NAME', '')
#                         Patient_Name = Patient_Name.replace('Patient', '')
#                         Patient_Name = Patient_Name.replace('atient Name', '')
#                         if Patient_Name:
#                             i['extraction'] = Patient_Name or i['extraction']
#                     if i['label'] == 'Patient_Acct_No':
#                         Patient_Acct_No = re.sub(r'^.*?(#:|number|ACCT|Acct|:|ACNT|Acnt)', '', i['extraction'])
#                         Patient_Acct_No = Patient_Acct_No.replace('Patient ID', '')
#                         Patient_Acct_No = Patient_Acct_No.replace('Patient Account', '')
#                         Patient_Acct_No = Patient_Acct_No.replace('Patient Acoounts', '')
#                         if Patient_Acct_No:
#                             i['extraction'] = Patient_Acct_No or i['extraction']
#                     if i['label'] == 'Group_No':
#                         Group_No = get_numeric_info(i['extraction'])
#                         if Group_No:
#                             i['extraction'] = Group_No[0] or i['extraction']
#                             i['extraction'] = get_trim_info(i['extraction'])[0]
#                             i['extraction'] = re.sub(r'^.*?(#:|number|:|Number:)', '', i['extraction'])
#
#                     if i['label'] == 'Provider_NPI':
#                         Provider_NPI = re.sub(r'^.*?(#:|number|NPL|NPI|:|#|PI|Provider|provider|der)', '', i['extraction'])
#                         i['extraction'] = Provider_NPI or i['extraction']
#
#                     if i['label'] == 'PT_RESP':
#                         PT_RESP = re.sub(r'^.*?(#:|RESP|PT|:#|:|#)', '', i['extraction'])
#                         i['extraction'] = PT_RESP or i['extraction']
#                     if i['label'] == 'Provider_Name':
#                         Provider_Name = re.sub('\W+', ' ', i['extraction'])
#                         if Provider_Name:
#                             i['extraction'] = Provider_Name or i['extraction']
#
#                     if i['label'] == 'Payer_Name':
#                         client_name = i['extraction']
#                         Payer_Name = re.sub('\W+', ' ', i['extraction'])
#                         Payer_Name = re.sub(r'^.*?(FROM|:|#:|TO|FROM:)', '', Payer_Name)
#                         Payer_Name = Payer_Name.replace('SUBJECT', '')
#                         if Payer_Name:
#                             i['extraction'] = Payer_Name or i['extraction']
#                             client_name = i['extraction']
#
#                     if i['label'] == 'Payee_ID':
#                         Payee_ID = re.sub('\W+', ' ', i['extraction'])
#                         Payee_ID = re.sub(r'^.*?(PAYEE ID:|:|#:|ID|PAYEE:)', '', Payee_ID)
#                         Payee_ID = Payee_ID.replace('SUBJECT', '')
#                         if Payee_ID:
#                             i['extraction'] = Payee_ID or i['extraction']
#
#
#             # final_dfs = [item for sublist in final_dfs for item in sublist]
#             # for d_f in final_dfs:
#             #     str_io = io.StringIO()
#             #     d_f.to_html(buf=str_io, classes='table table-striped')
#             #     html_str = str_io.getvalue()
#             #     html_df.append(html_str)
#             # html_df = list(set(html_df))
#
#             # for d in result_kvp:
#             #     del d['prob']
#             try:
#                 if 'WAYSTAR' in filename:
#                     waystar_total_claim_val = waystar_total_claim_info(text)
#                     for i in waystar_total_claim_val:
#                         result_kvp.append(i)
#             except Exception as e:
#                 print(e)
#             result_kvp = [dict(t) for t in {tuple(d.items()) for d in result_kvp}]
#
#         #     dict_ls = []
#         #     for i in final_dfs:
#         #         i = i.replace(np.nan, '', regex=True)
#         #         dict_l = i.to_dict('list')
#         #         dict_ls.append(dict_l)
#         #     for i in dict_ls:
#         #         for dct in i:
#         #             if dct == 'PATIENT NAME':
#         #                 i[dct] = list(filter(None, i[dct]))
#         #                 result_kvp.append({'label': 'Patient_Name', 'extraction': i[dct][0]})
#         #             # if dct == 'Provider NPI':
#         #             #     i[dct] = list(filter(None, i[dct]))
#         #             #     result_kvp.append({'label': 'Claim_Id', 'extraction': i[dct][0]})
#         #     for i in dict_ls:
#         #         if isinstance(i, dict):
#         #             new_dct = {}
#         #             for dct in i:
#         #                 i[dct] = list(filter(None, i[dct]))
#         #                 for key in result_kvp:
#         #                     if key['extraction'] == i['PATIENT NAME'][0]:
#         #                         new_dct[key['extraction']] = i
#         #             if new_dct:
#         #                 patient_list.append(new_dct)
#         #     # patient_name = []
#         #     for patient in patient_list:
#         #         for pat_name in patient:
#         #             patient_name.append(pat_name)
#         #     # for d in result_kvp:
#         #     #     if d['label'] == 'Patient_Name':
#         #     #         result_kvp.remove(d)
#         #     result_kvp_new = []
#         #     for patient in patient_list:
#         #         result_kv_dict = {}
#         #         for pat_name in patient:
#         #             value_dt = [
#         #                 # {'label': 'Claim_Id', 'extraction': patient[pat_name]['Provider NPI'][0] or 'None'},
#         #                 {'label': 'Patient_Name', 'extraction': pat_name or None},
#         #                 {'label': 'Policy_Certificate', 'extraction': patient[pat_name]['Policy/Certificate'][0] or None},
#         #                 {'label': 'Patient_Acct_No', 'extraction': patient[pat_name]['Patient Account'][0] or None},
#         #                 {'label': 'Claim_Total', 'extraction': patient[pat_name]['Claim Total'][0] or None},
#         #                 result_kvp
#         #             ]
#         #
#         #             result_kv_dict[pat_name] = value_dt
#         #
#         #         result_kvp_new.append(result_kv_dict)
#         #
#         #     final_list_kvp = []
#         #     for dt in result_kvp_new:
#         #         for dctt in dt:
#         #             final_list_kvp.append(dt[dctt])
#         #
#         #     if len(final_list_kvp) == 1:
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#         #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#         #         form_text_1 = list(final_list_kvp[0])
#         #     elif len(final_list_kvp) == 2:
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#         #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#         #         form_text_1 = list(final_list_kvp[0])
#         #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#         #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#         #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#         #         form_text_2 = list(final_list_kvp[1])
#         #     elif len(final_list_kvp) == 3:
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#         #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#         #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#         #         form_text_1 = list(final_list_kvp[0])
#         #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#         #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#         #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#         #         form_text_2 = list(final_list_kvp[1])
#         #         final_list_kvp[2] = str(final_list_kvp[2]).replace('[', '')
#         #         final_list_kvp[2] = str(final_list_kvp[2]).replace(']', '')
#         #         final_list_kvp[2] = ast.literal_eval(final_list_kvp[2])
#         #         form_text_3 = list(final_list_kvp[2])
#         #     else:
#         #         result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#         #         form_text_1 = result_kvp_1[0]
#         #         form_text_2 = result_kvp_1[1]
#         #         form_text_3 = result_kvp_1[2]
#         #
#         # final_df_list = []
#         # df = pd.DataFrame()
#         # for i in patient_list:
#         #     for d in i:
#         #         df = pd.DataFrame.from_dict(i[d], orient='index')
#         #         df = df.transpose()
#         #         final_df_list.append(df)
#         #
#         # result_df_list = []
#         # for f_df in final_df_list:
#         #     new_col = pd.DataFrame()
#         #     new_col['Patient_Name'] = f_df['PATIENT NAME']
#         #     new_col['SERV_DATE_FROM'] = f_df['DATE OF SERVICE']
#         #     new_col['SERV_DATE_TO'] = f_df['DATE OF SERVICE']
#         #     new_col['PROC'] = f_df['PROCEDURE CODE']
#         #     new_col['MODS'] = f_df['MEDICARE APPROVED']
#         #     new_col['BILLED/Charged'] = f_df['AMOUNT PAID']
#         #     new_col['DEDUCTIBLE'] = f_df['DEDUCTIBLE AMOUNT']
#         #     new_col['COPAY'] = f_df['CO-PAY AMOUNT']
#         #     new_col['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
#         #     new_col['GRP_RC_AMT'] = pd.Series([None])
#         #     new_col['PROV_PD'] = pd.Series([None])
#         #     new_col['Patient_responsibility'] = pd.Series([None])
#         #     new_col['ALLOWED'] = pd.Series([None])
#         #     new_col['No Of Unit/Quantity'] = pd.Series([None])
#         #     result_df_list.append(new_col)
#         #
#         # for d_f in result_df_list:
#         #     str_io = io.StringIO()
#         #     d_f.to_html(buf=str_io, classes='table table-striped')
#         #     html_str = str_io.getvalue()
#         #     html_df.append(html_str)
#         # html_df = list(set(html_df))
#             result_kvp= sorted(result_kvp, key=lambda i: i['label'])
#             result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#             form_text_1 = result_kvp_1[0]
#             form_text_2 = result_kvp_1[1]
#             form_text_3 = result_kvp_1[2]
#
#             if form_text_1:
#                 form_text_1 = clean_dict(form_text_1)
#                 form_text_1 = [dict(t) for t in {tuple(d.items()) for d in form_text_1}]
#             if form_text_2:
#                 form_text_2 = clean_dict(form_text_2)
#                 form_text_2 = [dict(t) for t in {tuple(d.items()) for d in form_text_2}]
#             if form_text_3:
#                 form_text_3 = clean_dict(form_text_3)
#                 form_text_3 = [dict(t) for t in {tuple(d.items()) for d in form_text_3}]
#
#             dir_name = path + "/"
#             test = os.listdir(dir_name)
#             for item in test:
#                 if item.endswith(".pdf"):
#                     os.remove(os.path.join(dir_name, item))
#             try:
#                 if os.path.exists(path + '/output.csv'):
#                     if page_format == 'Aetna_2':
#                         table_json, list_of_new_dfs = aetna_2_table_df(path + '/output.csv')
#                     elif page_format == 'Aetna_3':
#                         table_json, list_of_new_dfs = aetna_3_table_df(path + '/output.csv', pat_info_table_index)
#                     elif page_format == 'Aetna_1':
#                         table_json, list_of_new_dfs = aetna_1_table_df(path + '/output.csv')
#                     elif page_format == 'Aetna_4':
#                         table_json, list_of_new_dfs = aetna_4_table_df(path + '/output.csv')
#                     elif page_format == 'CIGNA_3':
#                         table_json, list_of_new_dfs = cigna_3_table_df(path + '/output.csv')
#                     elif page_format == 'CIGNA_2':
#                         table_json, list_of_new_dfs = cigna_2_table_df(path + '/output.csv')
#                     elif page_format == 'CIGNA_4':
#                         table_json, list_of_new_dfs = cigna_4_table_df(path + '/output.csv')
#                     elif page_format == 'UHC_1':
#                         table_json, list_of_new_dfs = uhc_1_table_df(path + '/output.csv')
#                     elif page_format == 'UHC_5':
#                         table_json, list_of_new_dfs = uhc_5_table_df(path + '/output.csv')
#                     else:
#                         table_json, list_of_new_dfs = common_format_table_df(path + '/output.csv')
#                         # df = pd.read_csv(path + '/output.csv')
#                         # table_json = df.to_json(orient="records")
#
#                 KvpJsonObj = KvpJson(file_name=filename, client_name=client_name,
#                                      claim_data=json.dumps(result_kvp),
#                                      table_data=json.dumps(table_json))
#                 KvpJsonObj.save()
#                 print("-----------File_Info_Saved---------")
#             except Exception as e:
#                 print(e)
#             # for d_f in list_of_new_dfs:
#             #     str_io = io.StringIO()
#             #     d_f.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
#             #     html_str = str_io.getvalue()
#             #     html_df.append(html_str)
#             # # html_df = list(set(html_df))
#             # if page_format == 'Aetna_2':
#             #     sequence_table_info = my_zip_longest((patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df), (patient_name_l_1[0], claim_id_l_1[0], patient_acct_no_l_1[0], html_df[0]))
#             #     # sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df,
#             #     #                                   fillvalue=patient_name_l_1[0])
#             # elif page_format == 'Aetna_3':
#             #     sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1,patient_acct_no_l_1, html_df, fillvalue="")
#             # else:
#             #     sequence_table_info = zip_longest([patient_name_l_1[0]], [claim_id_l_1[0]], [patient_acct_no_l_1[0]], html_df,
#             #                                       fillvalue="")
#     # return render(request, 'home.html', {
#     #     "csv_file": csv_file,
#     #     "form_text_1": form_text_1,
#     #     "form_text_2": form_text_2,
#     #     "form_text_3": form_text_3,
#     #     "file_name": filename,
#     #     "dfs": sequence_table_info,
#     #     # "patient_name": patient_name,
#     #     # "claim_id": claim_id
#     # })
#         # except Exception as e:
#         #     print(e)
#     return render(request, 'home.html')



# For fileupload and single file process
# def home(request):
#     """
#     :param request:
#     :return:
#     """
#     # try:
#     table_json = []
#     client_name = ''
#     page_format = ''
#     final_dfs = []
#     text = ''
#     config = '--psm 6 -c tessedit_char_blacklist=`~!@%^&*()£_-“+={[}}|;‘©,<>?”'
#     csv_file = 'output.csv'
#     html_df = []
#     list_of_new_dfs = []
#     # patient_name = ['Patient Name']
#     # claim_id = 'Claim id'
#     # client_lable = ['aetna', 'cigna', 'unitedhealthcare']
#     # SCALE = 9
#     # try:
#     if os.path.exists(path + '/output.csv'):
#         os.remove(path + '/output.csv')
#     # filelist = glob.glob(os.path.join(path + "/crop_image/", "*.*"))
#     # for f in filelist:
#     #     os.remove(f)
#     if request.method == 'POST' and request.FILES['files']:
#         myfile = request.FILES['files']
#         fs = FileSystemStorage()
#         filename = fs.save(myfile.name, myfile)
#         files = path + '/' + filename
#         file_type = filetype.guess(files)
#         image_or_file = ''
#         pat_info_list = []
#         if file_type.extension == 'pdf':
#             # with open(files, 'rb') as pdf:
#             pages3 = convert_from_bytes(open(files, 'rb').read())
#             # first_two_page = pages3[:2]
#             # first_page_text = ''
#             # for first_page in first_two_page:
#             #     first_page.save(path + '/first_page.jpg', 'jpeg')
#             #     first_page_name = path + '/first_page.jpg'
#             #     img = np.array(Image.open(first_page_name))
#             #     first_page_text += pytesseract.image_to_string(img, lang='eng', config='--psm 3')
#             # client_name = ''
#             # for i in client_lable:
#             #     found = re.search(i, first_page_text.lower())
#             #     if found:
#             #         client_name = found.group()
#             # # model_name = ''
#             # # if client_name == 'aetna':
#             # #     model_name = 'aetna'
#             # # elif client_name == 'cigna':
#             # #     model_name = 'cigna'
#             # # elif client_name == 'unitedhealthcare':
#             # #     model_name = 'unitedhealthcare'
#             page_format = pdf_format(files)
#             # print('page_format=============', page_format)
#             for page in pages3:
#                 page.save(path + '/converted.jpg', 'jpeg')
#                 documentName = path + '/converted.jpg'
#
#                 # dict_1 = text_extract_v1(documentName, model_name)
#                 try:
#                     degree = pytesseract.image_to_osd(Image.open(documentName))
#                     if "Rotate: 90" in degree:
#                         SCALE = 20
#                         src = cv2.imread(documentName)
#                         img = cv2.rotate(src, cv2.cv2.ROTATE_90_CLOCKWISE)
#                         plt.imsave(path + "/Py_Rotated_image.png", img)
#                         rotate_image = path + "/Py_Rotated_image.png"
#                     elif "Rotate: 270" in degree:
#                         src = cv2.imread(documentName)
#                         img = cv2.rotate(src, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)
#                         plt.imsave(path + "/Py_Rotated_image.png", img)
#                         rotate_image = path + "/Py_Rotated_image.png"
#                     else:
#                         rotate_image = documentName
#                 except Exception as e:
#                     print(e)
#                     rotate_image = documentName
#                 input_noteshrink = argparse.Namespace(basename='page',
#                                                       filenames=[rotate_image],
#                                                       global_palette=False, num_colors=8,
#                                                       postprocess_cmd=None, quiet=False,
#                                                       sample_fraction=0.05, sat_threshold=0.2,
#                                                       saturate=True, sort_numerically=True,
#                                                       value_threshold=0.25, white_bg=False)
#
#                 output_filename = notescan_main(input_noteshrink)
#                 try:
#                     # img_for_table = table_dark_img_procesing(rotate_image)
#                     # plt.imsave(path + "/table_input_image.png", img_for_table)
#                     # img_for_table1 = path + "/table_input_image.png"
#                     if page_format == 'Aetna_2':
#                         table__, pat_infoo = atena_2_table_textract(rotate_image)
#                         # pat_info_list.append(pat_infoo)
#                     elif page_format == 'Aetna_3':
#                         table__, pat_infoo = atena_3_table_textract(rotate_image)
#                         pat_info_list.append(pat_infoo)
#                     elif page_format == 'Aetna_1':
#                         aetna_1_table_textract(rotate_image)
#                     elif page_format == 'Aetna_4':
#                         aetna_4_table_textract(rotate_image)
#                     elif page_format == 'CIGNA_3':
#                         cigna_3_table_textract(rotate_image)
#
#                     elif page_format == 'CIGNA_2':
#                         cigna_2_table_textract(rotate_image)
#                     elif page_format == 'CIGNA_4':
#                         cigna_4_table_textract(rotate_image)
#                     elif page_format == 'UHC_1':
#                         uhc_1_table_textract(rotate_image)
#                     elif page_format == 'UHC_5':
#                         uhc_5_table_textract(rotate_image)
#                     else:
#                         table_textract(rotate_image)
#                 except Exception as e:
#                     print(e)
#                 # if page_format == 'Aetna_3':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'Aetna_2':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'Aetna_5':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'CIGNA_2':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'CIGNA_4':
#                 #     img = dark_img_procesing(rotate_image)
#                 # elif page_format == 'BCBS_1':
#                 #     img = dark_img_procesing(output_filename)
#                 # elif page_format == 'UHC_3':
#                 #     img = output_filename
#                 # else:
#                 #     # ######## img = np.array(Image.open(output_filename))
#                 #     img = dark_img_procesing(output_filename)
#                 # text += pytesseract.image_to_string(img, lang='eng', config=config)
#                 try:
#                     text += textrect(output_filename)
#                 except Exception as e:
#                     print(e)
#
#                 # crop_image_list = tabnet_res(output_filename)
#                 df_lists = []
#                 # for crop_img in crop_image_list:
#                 #     table_df = extract_cell_images_from_table(crop_img, SCALE)
#                 #     df = df_structure(table_df)
#                 #     df_lists.append(df)
#                 final_dfs.append(df_lists)
#
#                 # if dict_1:
#                 #     # final_list.append([{'label': 'Page No: ' + str(pages3.index(page) + 1),
#                 #     #                     'prob': '',
#                 #     #                     'extraction': ''}])
#                 #     final_list.append(dict_1['preds'])
#                 # else:
#                 #     final_list.append({})
#         # result_kvp = list(itertools.chain.from_iterable(final_list))
#         # text = text_correction(text)
#         text = text.replace(':\n', ': ')
#         result_kvp, patient_name_l_1, claim_id_l_1, patient_acct_no_l_1 = find_formate(text, page_format)
#
#         pat_info_table_index = []
#         # GEt Table Sequence By Format
#         if page_format == 'Aetna_3':
#             pat_info1 = [item for sublist in pat_info_list for item in sublist]
#             all_data = patient_name_l_1 + claim_id_l_1 + patient_acct_no_l_1
#             pat_info2 = []
#             for i in pat_info1:
#                 i = i.replace('Patient Name:', ' ')
#                 i = i.replace('Patient Account:', ' ')
#                 i = i.replace('Patient Account:', ' ')
#                 i = i.replace('Patient Accounta', ' ')
#                 m = i.split()
#                 for j in m:
#                     for k in all_data:
#                         if editdistance.eval(k, j) <= 3:
#                             i = i.replace(j, k)
#                 pat_info2.append(i)
#
#             index_dict = {}
#             count = 0
#             for i, j, k in zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, fillvalue=""):
#                 index_dict[count] = [i, j, k]
#                 count += 1
#             for i in pat_info2:
#                 for j in index_dict:
#                     for k in index_dict[j]:
#                         if k in i:
#                             if j not in pat_info_table_index:
#                                 pat_info_table_index.append(j)
#
#         for i in result_kvp:
#             if i['extraction'] is not None:
#                 if i['extraction'] == '':
#                     val = get_key_info(text, i['label'])
#                     if val:
#                         i['extraction'] = val[0] or i['extraction']
#                 if i['label'] == 'Processed_Date' or i['label'] == 'Date':
#                     date = get_date_info(i['extraction'])
#                     if date:
#                         i['extraction'] = date[0] or i['extraction']
#                         i['extraction'] = re.sub(r'^.*?(Date|:|DATE:|Date:|DATE)', '', i['extraction'])
#                 if i['label'] == 'Check_No':
#                     check_no = get_numeric_info(i['extraction'])
#                     if check_no:
#                         i['extraction'] = check_no[0] or i['extraction']
#                         i['extraction'] = get_trim_info(i['extraction'])[0]
#                 if i['label'] == 'Check_Amount':
#                     check_amt = get_float_info(i['extraction'])
#                     if check_amt:
#                         i['extraction'] = check_amt[0] or i['extraction']
#                 if i['label'] == 'Patient_Mem_ID':
#                     patient_mem_id = get_float_info(i['extraction'])
#                     if patient_mem_id:
#                         i['extraction'] = patient_mem_id[0] or i['extraction']
#                         i['extraction'] = get_trim_info(i['extraction'])[0]
#                 if i['label'] == 'Claim_Id':
#                     if page_format == 'UHC_1':
#                         Claim_Id = get_numeric_info_uhc_1(i['extraction'])
#                         i['extraction'] = Claim_Id[0] or i['extraction']
#                         i['extraction'] = i['extraction'].replace('SACKETT', '')
#                         i['extraction'] = i['extraction'].replace('SACKETT', '')
#                     else:
#                         Claim_Id = get_numeric_info(i['extraction'])
#                         if Claim_Id:
#                             i['extraction'] = Claim_Id[0] or i['extraction']
#                             i['extraction'] = get_trim_info(i['extraction'])[0]
#                 if i['label'] == 'Patient_Name':
#                     Patient_Name = re.sub('\W+', ' ', i['extraction'])
#                     Patient_Name = re.sub(r'^.*?(Name|name|NAME|PATIENT|Patient)', '', Patient_Name)
#                     Patient_Name = Patient_Name.replace('PATIENT NAME', '')
#                     Patient_Name = Patient_Name.replace('Patient', '')
#                     Patient_Name = Patient_Name.replace('atient Name', '')
#                     if Patient_Name:
#                         i['extraction'] = Patient_Name or i['extraction']
#                 if i['label'] == 'Patient_Acct_No':
#                     Patient_Acct_No = re.sub(r'^.*?(#:|number|ACCT|Acct|:|ACNT|Acnt)', '', i['extraction'])
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient ID', '')
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient Account', '')
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient Acoounts', '')
#                     if Patient_Acct_No:
#                         i['extraction'] = Patient_Acct_No or i['extraction']
#                 if i['label'] == 'Group_No':
#                     Group_No = get_numeric_info(i['extraction'])
#                     if Group_No:
#                         i['extraction'] = Group_No[0] or i['extraction']
#                         i['extraction'] = get_trim_info(i['extraction'])[0]
#                         i['extraction'] = re.sub(r'^.*?(#:|number|:|Number:)', '', i['extraction'])
#
#                 if i['label'] == 'Provider_NPI':
#                     Provider_NPI = re.sub(r'^.*?(#:|number|NPL|NPI|:|#|PI|Provider|provider|der)', '', i['extraction'])
#                     i['extraction'] = Provider_NPI or i['extraction']
#
#                 if i['label'] == 'PT_RESP':
#                     PT_RESP = re.sub(r'^.*?(#:|RESP|PT|:#|:|#)', '', i['extraction'])
#                     i['extraction'] = PT_RESP or i['extraction']
#                 if i['label'] == 'Provider_Name':
#                     Provider_Name = re.sub('\W+', ' ', i['extraction'])
#                     if Provider_Name:
#                         i['extraction'] = Provider_Name or i['extraction']
#
#                 if i['label'] == 'Payer_Name':
#                     client_name = i['extraction']
#                     Payer_Name = re.sub('\W+', ' ', i['extraction'])
#                     Payer_Name = re.sub(r'^.*?(FROM|:|#:|TO|FROM:)', '', Payer_Name)
#                     Payer_Name = Payer_Name.replace('SUBJECT', '')
#                     if Payer_Name:
#                         i['extraction'] = Payer_Name or i['extraction']
#                         client_name = i['extraction']
#
#                 if i['label'] == 'Payee_ID':
#                     Payee_ID = re.sub('\W+', ' ', i['extraction'])
#                     Payee_ID = re.sub(r'^.*?(PAYEE ID:|:|#:|ID|PAYEE:)', '', Payee_ID)
#                     Payee_ID = Payee_ID.replace('SUBJECT', '')
#                     if Payee_ID:
#                         i['extraction'] = Payee_ID or i['extraction']
#
#
#         # final_dfs = [item for sublist in final_dfs for item in sublist]
#         # for d_f in final_dfs:
#         #     str_io = io.StringIO()
#         #     d_f.to_html(buf=str_io, classes='table table-striped')
#         #     html_str = str_io.getvalue()
#         #     html_df.append(html_str)
#         # html_df = list(set(html_df))
#
#         # for d in result_kvp:
#         #     del d['prob']
#         try:
#             if 'WAYSTAR' in filename:
#                 waystar_total_claim_val = waystar_total_claim_info(text)
#                 for i in waystar_total_claim_val:
#                     result_kvp.append(i)
#         except Exception as e:
#             print(e)
#         result_kvp = [dict(t) for t in {tuple(d.items()) for d in result_kvp}]
#
#     #     dict_ls = []
#     #     for i in final_dfs:
#     #         i = i.replace(np.nan, '', regex=True)
#     #         dict_l = i.to_dict('list')
#     #         dict_ls.append(dict_l)
#     #     for i in dict_ls:
#     #         for dct in i:
#     #             if dct == 'PATIENT NAME':
#     #                 i[dct] = list(filter(None, i[dct]))
#     #                 result_kvp.append({'label': 'Patient_Name', 'extraction': i[dct][0]})
#     #             # if dct == 'Provider NPI':
#     #             #     i[dct] = list(filter(None, i[dct]))
#     #             #     result_kvp.append({'label': 'Claim_Id', 'extraction': i[dct][0]})
#     #     for i in dict_ls:
#     #         if isinstance(i, dict):
#     #             new_dct = {}
#     #             for dct in i:
#     #                 i[dct] = list(filter(None, i[dct]))
#     #                 for key in result_kvp:
#     #                     if key['extraction'] == i['PATIENT NAME'][0]:
#     #                         new_dct[key['extraction']] = i
#     #             if new_dct:
#     #                 patient_list.append(new_dct)
#     #     # patient_name = []
#     #     for patient in patient_list:
#     #         for pat_name in patient:
#     #             patient_name.append(pat_name)
#     #     # for d in result_kvp:
#     #     #     if d['label'] == 'Patient_Name':
#     #     #         result_kvp.remove(d)
#     #     result_kvp_new = []
#     #     for patient in patient_list:
#     #         result_kv_dict = {}
#     #         for pat_name in patient:
#     #             value_dt = [
#     #                 # {'label': 'Claim_Id', 'extraction': patient[pat_name]['Provider NPI'][0] or 'None'},
#     #                 {'label': 'Patient_Name', 'extraction': pat_name or None},
#     #                 {'label': 'Policy_Certificate', 'extraction': patient[pat_name]['Policy/Certificate'][0] or None},
#     #                 {'label': 'Patient_Acct_No', 'extraction': patient[pat_name]['Patient Account'][0] or None},
#     #                 {'label': 'Claim_Total', 'extraction': patient[pat_name]['Claim Total'][0] or None},
#     #                 result_kvp
#     #             ]
#     #
#     #             result_kv_dict[pat_name] = value_dt
#     #
#     #         result_kvp_new.append(result_kv_dict)
#     #
#     #     final_list_kvp = []
#     #     for dt in result_kvp_new:
#     #         for dctt in dt:
#     #             final_list_kvp.append(dt[dctt])
#     #
#     #     if len(final_list_kvp) == 1:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #     elif len(final_list_kvp) == 2:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#     #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#     #         form_text_2 = list(final_list_kvp[1])
#     #     elif len(final_list_kvp) == 3:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#     #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#     #         form_text_2 = list(final_list_kvp[1])
#     #         final_list_kvp[2] = str(final_list_kvp[2]).replace('[', '')
#     #         final_list_kvp[2] = str(final_list_kvp[2]).replace(']', '')
#     #         final_list_kvp[2] = ast.literal_eval(final_list_kvp[2])
#     #         form_text_3 = list(final_list_kvp[2])
#     #     else:
#     #         result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#     #         form_text_1 = result_kvp_1[0]
#     #         form_text_2 = result_kvp_1[1]
#     #         form_text_3 = result_kvp_1[2]
#     #
#     # final_df_list = []
#     # df = pd.DataFrame()
#     # for i in patient_list:
#     #     for d in i:
#     #         df = pd.DataFrame.from_dict(i[d], orient='index')
#     #         df = df.transpose()
#     #         final_df_list.append(df)
#     #
#     # result_df_list = []
#     # for f_df in final_df_list:
#     #     new_col = pd.DataFrame()
#     #     new_col['Patient_Name'] = f_df['PATIENT NAME']
#     #     new_col['SERV_DATE_FROM'] = f_df['DATE OF SERVICE']
#     #     new_col['SERV_DATE_TO'] = f_df['DATE OF SERVICE']
#     #     new_col['PROC'] = f_df['PROCEDURE CODE']
#     #     new_col['MODS'] = f_df['MEDICARE APPROVED']
#     #     new_col['BILLED/Charged'] = f_df['AMOUNT PAID']
#     #     new_col['DEDUCTIBLE'] = f_df['DEDUCTIBLE AMOUNT']
#     #     new_col['COPAY'] = f_df['CO-PAY AMOUNT']
#     #     new_col['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
#     #     new_col['GRP_RC_AMT'] = pd.Series([None])
#     #     new_col['PROV_PD'] = pd.Series([None])
#     #     new_col['Patient_responsibility'] = pd.Series([None])
#     #     new_col['ALLOWED'] = pd.Series([None])
#     #     new_col['No Of Unit/Quantity'] = pd.Series([None])
#     #     result_df_list.append(new_col)
#     #
#     # for d_f in result_df_list:
#     #     str_io = io.StringIO()
#     #     d_f.to_html(buf=str_io, classes='table table-striped')
#     #     html_str = str_io.getvalue()
#     #     html_df.append(html_str)
#     # html_df = list(set(html_df))
#         result_kvp= sorted(result_kvp, key=lambda i: i['label'])
#         result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#         form_text_1 = result_kvp_1[0]
#         form_text_2 = result_kvp_1[1]
#         form_text_3 = result_kvp_1[2]
#
#         if form_text_1:
#             form_text_1 = clean_dict(form_text_1)
#             form_text_1 = [dict(t) for t in {tuple(d.items()) for d in form_text_1}]
#         if form_text_2:
#             form_text_2 = clean_dict(form_text_2)
#             form_text_2 = [dict(t) for t in {tuple(d.items()) for d in form_text_2}]
#         if form_text_3:
#             form_text_3 = clean_dict(form_text_3)
#             form_text_3 = [dict(t) for t in {tuple(d.items()) for d in form_text_3}]
#
#         dir_name = path + "/"
#         test = os.listdir(dir_name)
#         for item in test:
#             if item.endswith(".pdf"):
#                 os.remove(os.path.join(dir_name, item))
#         try:
#             if os.path.exists(path + '/output.csv'):
#                 if page_format == 'Aetna_2':
#                     table_json, list_of_new_dfs = aetna_2_table_df(path + '/output.csv')
#                 elif page_format == 'Aetna_3':
#                     table_json, list_of_new_dfs = aetna_3_table_df(path + '/output.csv', pat_info_table_index)
#                 elif page_format == 'Aetna_1':
#                     table_json, list_of_new_dfs = aetna_1_table_df(path + '/output.csv')
#                 elif page_format == 'Aetna_4':
#                     table_json, list_of_new_dfs = aetna_4_table_df(path + '/output.csv')
#                 elif page_format == 'CIGNA_3':
#                     table_json, list_of_new_dfs = cigna_3_table_df(path + '/output.csv')
#                 elif page_format == 'CIGNA_2':
#                     table_json, list_of_new_dfs = cigna_2_table_df(path + '/output.csv')
#                 elif page_format == 'CIGNA_4':
#                     table_json, list_of_new_dfs = cigna_4_table_df(path + '/output.csv')
#                 elif page_format == 'UHC_1':
#                     table_json, list_of_new_dfs = uhc_1_table_df(path + '/output.csv')
#                 elif page_format == 'UHC_5':
#                     table_json, list_of_new_dfs = uhc_5_table_df(path + '/output.csv')
#                 else:
#                     table_json, list_of_new_dfs = common_format_table_df(path + '/output.csv')
#                     # df = pd.read_csv(path + '/output.csv')
#                     # table_json = df.to_json(orient="records")
#
#             KvpJsonObj = KvpJson(file_name=filename, client_name=client_name,
#                                  claim_data=json.dumps(result_kvp),
#                                  table_data=json.dumps(table_json))
#             KvpJsonObj.save()
#         except Exception as e:
#             print(e)
#         for d_f in list_of_new_dfs:
#             str_io = io.StringIO()
#             d_f.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
#             html_str = str_io.getvalue()
#             html_df.append(html_str)
#         # html_df = list(set(html_df))
#         if page_format == 'Aetna_2':
#             sequence_table_info = my_zip_longest((patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df), (patient_name_l_1[0], claim_id_l_1[0], patient_acct_no_l_1[0], html_df[0]))
#             # sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df,
#             #                                   fillvalue=patient_name_l_1[0])
#         elif page_format == 'Aetna_3':
#             sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1,patient_acct_no_l_1, html_df, fillvalue="")
#         else:
#             sequence_table_info = zip_longest([patient_name_l_1[0]], [claim_id_l_1[0]], [patient_acct_no_l_1[0]], html_df,
#                                               fillvalue="")
#         return render(request, 'home.html', {
#             "csv_file": csv_file,
#             "form_text_1": form_text_1,
#             "form_text_2": form_text_2,
#             "form_text_3": form_text_3,
#             "file_name": filename,
#             "dfs": sequence_table_info,
#             # "patient_name": patient_name,
#             # "claim_id": claim_id
#         })
#         # except Exception as e:
#         #     print(e)
#     return render(request, 'home.html')