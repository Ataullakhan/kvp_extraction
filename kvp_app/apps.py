from django.apps import AppConfig


class KvpAppConfig(AppConfig):
    name = 'kvp_app'
