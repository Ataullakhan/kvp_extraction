# from mmdet.apis import init_detector, inference_detector
# import cv2
# # import matplotlib.pyplot as plt
# import pytesseract
# import pandas as pd
# import numpy as np
# import re
# import editdistance
# from kvp_extraction import settings
#
# media_path = settings.MEDIA_ROOT
#
#
# config_file = media_path + '/tabnet_model/cascade_mask_rcnn_hrnetv2p_w32_20e_v3.py'
# checkpoint_file = media_path + '/tabnet_model/ICDAR.19.Track.B2.Modern.table.structure.recognition.v2.pth'
# model = init_detector(config_file, checkpoint_file, device='cpu')
#
#
# def extract_cell_images_from_table(image, SCALE):
#     # print(SCALE)
#     # final_df1 = pd.DataFrame()
#     final_df = dict()
#     try:
#         BLUR_KERNEL_SIZE = (17, 17)
#         STD_DEV_X_DIRECTION = 0
#         STD_DEV_Y_DIRECTION = 0
#
#         #     yen_threshold = threshold_yen(image)
#         #     image = rescale_intensity(image, (0, yen_threshold), (0, 255))
#         #     image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#         scale_c = 1.5
#         image = cv2.resize(image, (round(scale_c * image.shape[1]), round(scale_c * image.shape[0])))
#         image = np.array(image, dtype=np.uint8)
#         blurred = cv2.GaussianBlur(image, BLUR_KERNEL_SIZE, STD_DEV_X_DIRECTION, STD_DEV_Y_DIRECTION)
#         MAX_COLOR_VAL = 255
#         BLOCK_SIZE = 15
#         SUBTRACT_FROM_MEAN = -2
#         #     img_bin = cv2.threshold(blurred,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#
#         img_bin = cv2.adaptiveThreshold(
#             ~blurred,
#             MAX_COLOR_VAL,
#             cv2.ADAPTIVE_THRESH_MEAN_C,
#             cv2.THRESH_BINARY,
#             BLOCK_SIZE,
#             SUBTRACT_FROM_MEAN,
#         )
#         vertical = horizontal = img_bin.copy()
#         image_width, image_height = horizontal.shape
#         horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (int(image_width / SCALE), 1))
#         horizontally_opened = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, horizontal_kernel)
#         vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, int(image_height / SCALE)))
#         vertically_opened = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, vertical_kernel)
#
#         horizontally_dilated = cv2.dilate(horizontally_opened, cv2.getStructuringElement(cv2.MORPH_RECT, (40, 1)))
#         vertically_dilated = cv2.dilate(vertically_opened, cv2.getStructuringElement(cv2.MORPH_RECT, (1, 60)))
#
#         mask = horizontally_dilated + vertically_dilated
#         contours, heirarchy = cv2.findContours(
#             mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE,
#         )
#         perimeter_lengths = [cv2.arcLength(c, True) for c in contours]
#         epsilons = [0.05 * p for p in perimeter_lengths]
#         approx_polys = [cv2.approxPolyDP(c, e, True) for c, e in zip(contours, epsilons)]
#
#         # Filter out contours that aren't rectangular. Those that aren't rectangular
#         approx_rects = [p for p in approx_polys if len(p) == 4]
#         bounding_rects = [cv2.boundingRect(a) for a in approx_polys]
#
#         # Filter out rectangles that are too narrow or too short.
#         MIN_RECT_WIDTH = 40
#         MIN_RECT_HEIGHT = 10
#         bounding_rects = [
#             r for r in bounding_rects if MIN_RECT_WIDTH < r[2] and MIN_RECT_HEIGHT < r[3]
#         ]
#
#         # The largest bounding rectangle is assumed to be the entire table.
#
#         #     print(bounding_rects)
#         largest_rect = max(bounding_rects, key=lambda r: r[2] * r[3])
#
#         bounding_rects = [b for b in bounding_rects if b is not largest_rect]
#
#         cells = [c for c in bounding_rects]
#
#         def cell_in_same_row(c1, c2):
#             c1_center = c1[1] + c1[3] - c1[3] / 2
#             c2_bottom = c2[1] + c2[3]
#             c2_top = c2[1]
#             return c2_top < c1_center < c2_bottom
#
#         orig_cells = [c for c in cells]
#         rows = []
#         while cells:
#             first = cells[0]
#             rest = cells[1:]
#             cells_in_same_row = sorted(
#                 [
#                     c for c in rest
#                     if cell_in_same_row(c, first)
#                 ],
#                 key=lambda c: c[0]
#             )
#
#             row_cells = sorted([first] + cells_in_same_row, key=lambda c: c[0])
#             rows.append(row_cells)
#             cells = [
#                 c for c in rest
#                 if not cell_in_same_row(c, first)
#             ]
#
#         # Sort rows by average height of their center.
#         def avg_height_of_center(row):
#             centers = [y + h - h / 2 for x, y, w, h in row]
#             return sum(centers) / len(centers)
#
#         rows.sort(key=avg_height_of_center)
#         cell_images_rows = []
#         for row in rows:
#             cell_images_row = []
#             for x, y, w, h in row:
#                 cell_images_row.append(image[y:y + h, x:x + w])
#             cell_images_rows.append(cell_images_row)
#
#         for x, cell in enumerate(cell_images_rows):
#             #         print('col',x)
#             for i, c in enumerate(cell):
#                 scale_c = 1.5
#                 c = cv2.resize(c, (round(scale_c * c.shape[1]), round(scale_c * c.shape[0])))
#                 c = cv2.threshold(c, 127, 255, cv2.THRESH_TRUNC)
#                 c = 1, cv2.bilateralFilter(c[1], 7, sigmaSpace=100, sigmaColor=100)
#                 #             c = 1,cv2.adaptiveThreshold(c, 255,
#                 #                                     cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
#                 #                                     cv2.THRESH_BINARY, 7, 8)
#                 out = pytesseract.image_to_string(c[1], config='--oem 3 --psm 6')
#                 final_df.setdefault((x, i), out)
#     #         if final_df:
#     #             final_df1 = df_structure(final_df)
#     except Exception as e:
#         print(e)
#     return final_df
#
#
# def tabnet_res(img):
#     result = inference_detector(model, img)
#     res_borderTable = []
#     res_borderlessTable = []
#     res_cell = []
#     df_list = []
#     ## for tables with borders
#     for r in result[0][0]:
#         if r[4] > .85:
#             res_borderTable.append(r[:4].astype(int))
#         ## for cells
#     for r in result[0][1]:
#         if r[4] > .85:
#             r[4] = r[4] * 100
#             res_cell.append(r.astype(int))
#         ## for borderless tables
#     for r in result[0][2]:
#         if r[4] > .85:
#             res_borderlessTable.append(r[:4].astype(int))
#
#     img = cv2.imread(img)
#     crop_img_list = []
#     for i, val in enumerate(res_borderlessTable):
#         x_min, x_max = 0, img.shape[1]
#         y_min, y_max = val[1] - 150, val[3] + 20
#         crop_img = img[y_min:y_max, x_min:x_max]
#
#         gray = np.float64(crop_img)
#         noise = np.random.randn(*gray[1].shape) * 10
#         noisy = gray + noise
#         noisy = np.uint8(np.clip(noisy, 0, 255))
#         crop_img = cv2.fastNlMeansDenoisingColored(noisy, None, 10, 10, 7, 21)
#
#         crop_img = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
#
#         #         _, blackAndWhite = cv2.threshold(crop_img, 127, 255, cv2.THRESH_BINARY_INV)
#
#         #         nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(blackAndWhite, None, None, None, 8, cv2.CV_32S)
#         #         sizes = stats[1:, -1] #get CC_STAT_AREA component
#         #         img2 = np.zeros((labels.shape), np.uint8)
#
#         #         for i in range(0, nlabels - 1):
#         #             if sizes[i] >= 0:   #filter small dotted regions
#         #                 img2[labels == i + 1] = 255
#
#         #         res = cv2.bitwise_not(img2)
#
#         #         plt.imsave("cropped_image_1.jpg", crop_img, cmap='Greys_r')
#         #         table_df = extract_cell_images_from_table(crop_img)
#         #         print(table_df)
#         #         df_list.append(table_df)
#         crop_img_list.append(crop_img)
#
#     return crop_img_list
#
#
# def df_structure(input_dict):
#     col_nam_dict = {9:'PATIENT NAME',10:'Provider NPI',11:'Policy/Certificate',12:'Patient Account',13:'Claim Total',
#                    0:'DATE OF SERVICE',1:'PROCEDURE CODE',2:'ACTUAL EXPENSE',3:'MEDICARE APPROVED',
#                    4:'DEDUCTIBLE AMOUNT',5:'CO-PAY AMOUNT',6:'PATIENT MAY BE BILLED',7:'AMOUNT PAID',8:'REMARKS'}
#     final_df = pd.DataFrame()
#     new_dict = dict()
#     try:
#         dates_index = [i[0] for i in input_dict if re.findall('((?:[0-9]{2}/){2}[0-9]{4})',input_dict[i])]
#         for i in input_dict:
#             if not pd.isna(input_dict[i]):
#                 temp_d = dict()
#                 input_dict[i] = re.sub(r'[\x00-\x08\x0b\x0c\x0e-\x1f\x7f-\xff]', '', input_dict[i])
#                 if re.match('(?:[0-9]{2}/){2}[0-9]{2}',input_dict[i]):
#                     d_spilt = input_dict[i].replace('\s+','')
#                     d_spilt = d_spilt.split('\n')
#                     d_spilt = list(filter(None,d_spilt))
#                     dates_index = i
#                     for n,d in enumerate(d_spilt):
#                         new_dict.setdefault((i[0]+n,i[1]),d)
#
#                 elif re.match('.*Patient Name:|Claim.*',edit_distance(input_dict[i])):
#
#                     text = edit_distance(input_dict[i])
#                     p_name = re.findall('Patient Name:(\s+\S+\s+\S+|$)',text)[0].strip()
#                     p_npi = re.findall('Provider([a-zA-Z ]+\W+\d+)',text)[0].strip()
#                     p_pol = re.findall('Policy/Certificate(\s+\S+\s+\S+|$)',text)[0].strip()
#                     p_acc = re.findall('Patient Account:(\s+\S+\s+\S+|$)',text)[0].strip()
#                     cl_id = re.findall('Claim([a-zA-Z ]+\W+\d+)',text)[0].strip()
#
#     #                 cl_tot = re.findall('Claim Total Paid:*?(\s[-+]?\d*\.*\d+)',input_dict[i])[0].strip()
#                     new_dict.setdefault((i[0],max(input_dict.keys())[1]+1),p_name or [None])
#                     new_dict.setdefault((i[0],max(input_dict.keys())[1]+2),p_npi or [None])
#                     new_dict.setdefault((i[0],max(input_dict.keys())[1]+3),p_pol or [None])
#                     new_dict.setdefault((i[0],max(input_dict.keys())[1]+4),p_acc or [None])
#                     new_dict.setdefault((i[0],max(input_dict.keys())[1]+5),cl_id or [None])
#     #                 new_dict.setdefault((i[0],max(input_dict.keys())[1]+6),cl_tot)
#
#
#                 elif i[0] == dates_index[0]:
#                     oth_split = input_dict[i].replace('\s','')
#                     oth_split = oth_split.split('\n')
#                     oth_split = list(filter(None,oth_split))
#                     for o, oth in enumerate(oth_split):
#                         new_dict.setdefault((i[0]+o,i[1]),oth)
#                 else:
#                     new_dict.setdefault((i[0],i[1]),input_dict[i])
#         for i in new_dict:
#             final_df.loc[i[0], col_nam_dict[i[1]]] = new_dict[i]
#         to_col = [i for i in new_dict if re.match('.*DATE.*|.*MEDICARE.*|.*PROCEDURE.*|.*Service.*',new_dict[i])][0]
#         if to_col:
#             final_df = final_df.drop(to_col[0])
#         for k,v in col_nam_dict.items():
#             if v not in final_df.columns:
#                 final_df[v] = 'None'
#
#     except Exception as e:
#         print(e)
#     return final_df.dropna(how='all')
#
#
# def edit_distance(input_dict):
#     text = str(input_dict)
#     str_1 = re.findall(r"[\w']+", text)
#     for i in str_1:
#         if editdistance.eval('Patient', i) <= 3:
#             text = text.replace(i, 'Patient')
#         if editdistance.eval('Provider', i) <= 3:
#             text = text.replace(i, 'Provider')
#         if editdistance.eval('Claim', i) <= 2:
#             text = text.replace(i, 'Claim')
#         if editdistance.eval('Account', i) <= 3:
#             text = text.replace(i, 'Account')
#         if editdistance.eval('Name', i) <= 2:
#             text = text.replace(i, 'Name')
#     return text
#
#
# def get_claim_info(input_dict):
#     final_df = pd.DataFrame()
#     new_dict = dict()
#     text = edit_distance(input_dict)
#     patient_name = re.findall('Patient([a-zA-Z ]+\W+\S+\W+\S+)', text) or ["none"]
#     Provider_number = re.findall('Provider([a-zA-Z ]+\W+\d+)', text) or ["none"]
#     claim_number = re.findall('Claim([a-zA-Z ]+\W+\d+)', text) or ["none"]
#
#     new_dict.setdefault((0, max(input_dict.keys())[1] + 1), patient_name[0])
#     new_dict.setdefault((0, max(input_dict.keys())[1] + 2), Provider_number[0])
#     new_dict.setdefault((0, max(input_dict.keys())[1] + 3), claim_number[0])
#
#     new_dict = {**new_dict, **input_dict}
#     if new_dict is not None:
#         for i in new_dict:
#             final_df.loc[i[0], i[1]] = new_dict[i]
#
#     return final_df.dropna(how='all')
#
