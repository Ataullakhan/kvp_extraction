# import cv2
# import numpy as np
# from pytesseract import pytesseract
# import pytesseract
# from PIL import Image
# from kvp_app.utils import rotate, textrect, get_grayscale, remove_noise, thresholding, resize
# from kvp_extraction import settings
# from deskew import determine_skew
# # import time
# import matplotlib.pyplot as plt
#
#
# def image_processing(image, config):
#     # kernel = np.ones((1, 40), np.uint8)
#     # morphed = cv2.morphologyEx(np.array(image), cv2.MORPH_CLOSE, kernel)
#     # # (2) Invert the morphed image, and add to the source image:
#     # dst = cv2.add(np.array(image), (255 - morphed))
#     #
#     #
#     # scale_percent = 250  # percent of original size
#     # width = int(dst.shape[1] * scale_percent / 100)
#     # height = int(dst.shape[0] * scale_percent / 100)
#     # dim = (width, height)
#     # # resize image
#     # gray_img = cv2.resize(dst, dim, interpolation=cv2.INTER_AREA)
#     #
#     # gray_img = cv2.fastNlMeansDenoisingColored(gray_img,None,10,10,7,21)
#     #
#     # img_gray = cv2.cvtColor(gray_img, cv2.COLOR_BGR2GRAY)
#     im = cv2.bilateralFilter(image, 5, 55, 60)
#     im = get_grayscale(im)
#     # if config == '--oem 3 --psm 6  -c tessedit_char_whitelist=0123456789.':
#     #     im = resize(im)
#     # im = remove_noise(im)
#     im = thresholding(im)
#     angle = determine_skew(im)
#     if angle < -70:
#         rotated = rotate(im, angle, (0, 0, 0))
#     else:
#         rotated = im
#     img = Image.fromarray(rotated)
#
#     text = pytesseract.image_to_string(img, lang='eng', config=config)
#     return text
#
#
# path = settings.MEDIA_ROOT
# old_model = path + '/darknet_model/old_model/main_class_custom-yolov4-detector_last_v1.weights'
# new_aetna = path + '/darknet_model/New_Aetna/AETNA_custom-yolov4-detector_best.weights'
# cigna_model = path + '/darknet_model/CIGNA/custom-yolov4-detector_best.weights'
# uhc_model = path + '/darknet_model/UHC/UHC_yolov4-detector_best.weights'
#
# class Args:
#     # image = 'D:/Project/FreeFormText/Project_00/text_extraction/txt_results/'
#     old_config = path + '/darknet_model/old_model/main_class_custom-yolov4-detector.cfg'
#     old_weights = old_model
#     old_names = path + '/darknet_model/old_model/main_class_obj.names'
#
#     aetna_config = path + '/darknet_model/New_Aetna/AETNA-yolov4-detector.cfg'
#     aetna_weights = new_aetna
#     aetna_names = path + '/darknet_model/New_Aetna/AETNA_labels.names'
#
#     cigna_config = path + '/darknet_model/CIGNA/custom-yolov4-detector.cfg'
#     cigna_weights = cigna_model
#     cigna_name = path + '/darknet_model/CIGNA/obj.names'
#
#     uhc_config = path + '/darknet_model/UHC/UHC_custom-yolov4-detector.cfg'
#     uhc_weights = uhc_model
#     uhb_name = path + '/darknet_model/UHC/UHC_labels.names'
#
#
#
# args = Args()
#
# CONF_THRESH, NMS_THRESH = 0.5, 0.5
#
#
# def text_extract_v1(imag, model_name):
#     if model_name == 'aetna':
#         weights = args.aetna_weights
#         name = args.aetna_names
#         config = args.aetna_config
#     elif model_name == 'cigna':
#         weights = args.cigna_weights
#         name = args.cigna_name
#         config = args.cigna_config
#     elif model_name == 'unitedhealthcare':
#         weights = args.uhc_weights
#         name = args.uhb_name
#         config = args.uhc_config
#     else:
#         weights = args.old_weights
#         name = args.old_names
#         config = args.old_config
#     # Load Network
#     net = cv2.dnn.readNetFromDarknet(config, weights)
#     net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
#     net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
#
#     # Get the output layer from YOLO
#     layers = net.getLayerNames()
#     output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]
#
#     dict_cord = {}
#     dict_c = dict()
#     df_list = []
#     if 'jpg' in imag:
#         img = cv2.imread(imag)
#         height, width = img.shape[:2]
#
#         blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True, crop=False)
#         net.setInput(blob)
#         layer_outputs = net.forward(output_layers)
#         class_names_all = list(map(lambda s: s.strip(),open(name).readlines()))
#         class_names, confidences, b_boxes = [], [],[]
#         for output in layer_outputs:
#             try:
#                 for detection in output:
#                     scores = detection[5:]
#                     class_id = np.argmax(scores)
#                     confidence = scores[class_id]
#
#                     if confidence > CONF_THRESH:
#                         center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')
#
#                         x = int(center_x - w / 2)
#                         y = int(center_y - h / 2)
#
#                         b_boxes.append([x, y, int(w), int(h)])
#                         confidences.append(float(confidence))
#     #                         class_ids.append(int(class_id))
#                         class_names.append(class_names_all[class_id])
#             except:
#                 pass
#
#         for label, conf, b_box in zip(class_names, confidences,b_boxes):
#             label_ = label
#             x_min,y_min=int(b_box[0]),int(b_box[1])
#             x_max,y_max= x_min+int(b_box[2]), y_min+int(b_box[3])
#             x_scale,y_scale = (width/416),(height/416)
#             try:
#                 scl_y_min, scl_y_max = round(y_min*y_scale),round(y_max*y_scale)
#                 scl_x_min, scl_x_max = round(x_min*x_scale),round(x_max*x_scale)
#                 crop_img = img[scl_y_min:scl_y_max, scl_x_min:scl_x_max]
#                 ex_text = pytesseract.image_to_string(crop_img)
#                 dict_c.setdefault('preds',[]).append({'label': label_, 'prob': conf,'extraction': ex_text})
#             except Exception as e:
#                 if label_ == 'Table':
#                     crop_img = img[y_min:y_max, x_min:x_max]
#                     # plt.imsave("table_image.jpg", crop_img)
#                 if label_ != 'Table' and label_ != 'Remarks':
#                     crop_img = img[y_min:y_max, x_min:x_max]
#                     # plt.imsave("crop"+label_+'_{}.jpg'.format(class_names.index(label)), crop_img)
#                     if label_ == 'Check_Amount':
#                         py_config = '--oem 3 --psm 6  -c tessedit_char_whitelist=0123456789.'
#                         ex_text = image_processing(crop_img, py_config)
#                     elif label_ == 'Check_No':
#                         py_config = '--oem 3 --psm 6 -c tessedit_char_whitelist=0123456789CcHhEeKk#:'
#                         ex_text = image_processing(crop_img, py_config)
#                     elif label_ == 'Claim_Id':
#                         py_config = '--oem 3 --psm 6 -c tessedit_char_whitelist=0123456789CcLlAaIiMm#:'
#                         ex_text = image_processing(crop_img, py_config)
#                     elif label_ == 'Group_No':
#                         py_config = '--oem 3 --psm 6 -c tessedit_char_whitelist=PprRoOvViIdDeENn0123456789#:'
#                         ex_text = image_processing(crop_img, py_config)
#                     elif label_ == 'Patient_Name':
#                         py_config = '--oem 3 --psm 6 -c tessedit_char_blacklist=0123456789'
#                         ex_text = image_processing(crop_img, py_config)
#                     else:
#                         py_config = '--oem 3 --psm 6'
#                         ex_text = image_processing(crop_img, py_config)
#                     # success, encoded_image = cv2.imencode('.jpg', crop_img)
#                     # content2 = encoded_image.tobytes()
#                     # ex_text = textrect(content2)
#                     dict_c.setdefault('preds',[]).append({'label':label_,'prob':conf,'extraction':ex_text})
#                 # if label_ == 'full_table':
#                 #     im = Image.fromarray(img)
#                 #     im.save("tabnet_conv.jpg")
#                 #     img_1 = "tabnet_conv.jpg"
#                 #     table_df = get_table(img_1)
#                 #     df_list.append(table_df)
#     return dict_c