from kvp_extraction import settings
import re
# import os
# import argparse
import boto3
from PIL import Image
from PIL import ImageEnhance
from typing import Tuple, Union
import numpy as np
import cv2
import math
import pandas as pd

labels = [{'Check_Amount': ['Check Amount:']},
          {'Claim_Id': ['Claim #']},
          {'Patient_Acct_No': ['Patient Account:']},
          {'Patient_Name': ['Patient Name:']},
          {'Group_No': ['Group Number']},
          {'Check_No': ['Check #:']},
          {'Processed_Date': ['Date']}]

# all_labels = ['Allowable_Amt', 'Applied_Copay', 'Check_Amount', 'Check_No', 'Co_Insurance', 'Date_Service',
#               'Deductible',
#               'Group_No', 'Member', 'Not_Payable', 'Patient_Acct_No', 'Patient_Address', 'Patient_Mem_ID',
#               'Patient_Name',
#               'Payable_Amt', 'Pro_Rev_Code', 'Processed_Date', 'Sub_Amt_Bill_Charges', 'Table', 'Total_Charges']
path = settings.MEDIA_ROOT


# model = path + 'models/custom-yolov4-detector_best.weights'


# def text_extract(org_img):
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--image', default=org_img, help="image for prediction")
#     parser.add_argument('--config', default=path + '/darknet/cfg/custom-yolov4-detector.cfg', help="YOLO config path")
#     parser.add_argument('--weights', default=model, help="YOLO weights path")
#     parser.add_argument('--names', default=path + '/darknet/data/coco.names', help="class names path")
#     args = parser.parse_args()
#
#     CONF_THRESH, NMS_THRESH = 0.5, 0.5
#
#     # Load the network
#     net = cv2.dnn.readNetFromDarknet(args.config, args.weights)
#     net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
#     net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
#
#     # Get the output layer from YOLO
#     layers = net.getLayerNames()
#     output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]
#
#     img = cv2.imread(args.image)
#     height, width = img.shape[:2]
#
#     blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True, crop=False)
#     net.setInput(blob)
#     layer_outputs = net.forward(output_layers)
#
#     class_ids, confidences, b_boxes = [], [], []
#     for output in layer_outputs:
#         for detection in output:
#             scores = detection[5:]
#             class_id = np.argmax(scores)
#             confidence = scores[class_id]
#
#             if confidence > CONF_THRESH:
#                 center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')
#
#                 x = int(center_x - w / 2)
#                 y = int(center_y - h / 2)
#
#                 b_boxes.append([x, y, int(w), int(h)])
#                 confidences.append(float(confidence))
#                 class_ids.append(int(class_id))
#
#     return class_ids, confidences, b_boxes


def rotate(
        image: np.ndarray, angle: float, background: Union[int, Tuple[int, int, int]]
) -> np.ndarray:
    old_width, old_height = image.shape[:2]
    angle_radian = math.radians(angle)
    width = abs(np.sin(angle_radian) * old_height) + abs(np.cos(angle_radian) * old_width)
    height = abs(np.sin(angle_radian) * old_width) + abs(np.cos(angle_radian) * old_height)

    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rot_mat[1, 2] += (width - old_width) / 2
    rot_mat[0, 2] += (height - old_height) / 2
    return cv2.warpAffine(image, rot_mat, (int(round(height)), int(round(width))), borderValue=background)


def get_date_info(text):
    date_reg_exp2 = re.compile(r'\d{2}([-/.])(\d{2}|[a-zA-Z]{3})\1(\d{4}|\d{2})|\w{2,12}\s\d{2}[,.]\s\d{4}')
    matches_list = [x.group() for x in date_reg_exp2.finditer(text)]
    return matches_list


def get_numeric_info(text):
    info = []
    if text.find("Cla") != -1:
        pattern = 'Cla(.+)'
    elif text.find("Che") != -1:
        pattern = 'Che(.+)'
    elif text.find("NPI") != -1:
        pattern = 'NP(.+)'
    elif text.find("EFT") != -1:
        pattern = 'EFT(.+)'
    elif text.find("ICN") != -1:
        pattern = 'ICN(.+)'
    else:
        pattern = '([A-Z]{1,6}\d(.+)|\d(.+))'

    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    return info


def get_numeric_info_uhc_1(text):
    info = []
    if text.find("Cla") != -1:
        pattern = 'Cla(.+)'
    else:
        pattern = '([a-zA-Z]{2}\d+\s\d+)'

    if re.search(pattern, text, re.IGNORECASE) is None:
        pattern = '(.+)'

    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    return info


def get_trim_info(text):
    info = []

    pattern = '([A-Z]{1,6}\d(.+)|\d(.+))'

    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    if not info:
        info.append("")
    return info


def get_float_info(text):
    info = []
    pattern = '((\d+\.\d+)|(\d+)|(\d+\:\d+)|(\w+\d+)|(\S+))'
    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    return info


def get_patient_name(text):
    info = []
    pattern = '\d+\.\d+|\d+'
    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    return info


black = (0, 0, 0)
white = (255, 255, 255)
threshold = (160, 160, 160)
remove_cell_keyword = ['name', 'patient', 'npi', 'claim', 'provider', 'account', 'policy']


def adjust_sharpness(input_image, output_image, factor):
    image = Image.open(input_image)
    enhancer_object = ImageEnhance.Sharpness(image)
    out = enhancer_object.enhance(factor)
    out.save(output_image)


def table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv_1 = generate_table_csv(table, blocks_map)
            if 'Identifier' in csv_1:
                csv += ''
            else:
                csv += csv_1
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        # print(rows.items())
        for i in rows.items():
            for j in i[1]:
                if 'Statement Summary' in i[1][j] or \
                        '# OF CLAIMS 1' in i[1][j] or \
                        'Statement Totals' in i[1][j] or\
                        'Other Insurance' in i[1][j] or\
                        'Bill Type Code' in i[1][j]:
                    rows = {}

        csv = ''
        # remove_cell_keyword_list = ['name', 'carrier', 'prov npi:', 'policy',
        #                             'subscriber', 'mrn', 'member', 'corrected',
        #                             'patient', 'provider', 'tex id:', 'number',
        #                             'items', 'line', 'claim #', 'group #:']
        for row_index, cols in rows.items():
            if len(cols.items()) > 6:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    text = str(text)
                    if 'NAME' in text or 'CARRIER' in text or \
                            'PROV NPI:' in text or 'Policy' in text or \
                            'SUBSCRIBER:' in text or 'MRN' in text or \
                            'MEMBER:' in text or 'CORRECTED:' in text or \
                            'Patient' in text or \
                            'Tax ID:' in text or 'Member' in text or \
                            'Number:' in text or 'Items' in text or \
                            'Claim #' in text or \
                            'Group #:' in text or 'PROVIDER:' in text or \
                            'GROUP #:' in text or '#: 518668' in text or \
                            'CLAIM #' in text or 'MEDICARE' in text or \
                            'CAREMARK' in text or 'FINALIZED' in text or\
                            'CLAIM#:' in text or 'SUB ID:' in text or 'PATIENT:' in text:
                        break
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def textrect(image_file):
    """

    :return:
    """
    text3 = ""
    with open(image_file, 'rb') as document:
        imagebytes = bytearray(document.read())

    # Amazon Extract client
    textract = boto3.client('textract')
    # Call Amazon Extract
    response = textract.detect_document_text(Document={'Bytes': imagebytes})
    for item in response["Blocks"]:
        if item["BlockType"] == "LINE":
            text3 += item["Text"] + '\n'
    text3 += '\n'

    return text3


def forms_texract(image_file):
    """

    :param image_file:
    :return:
    """
    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_kv_map(image_file):

        with open(image_file, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['FORMS'])
        # Get the text blocks
        blocks = response['Blocks']

        # get key and value maps
        key_map = {}
        value_map = {}
        block_map = {}
        for block in blocks:
            block_id = block['Id']
            block_map[block_id] = block
            if block['BlockType'] == "KEY_VALUE_SET":
                if 'KEY' in block['EntityTypes']:
                    key_map[block_id] = block
                else:
                    value_map[block_id] = block

        return key_map, value_map, block_map

    def get_kv_relationship(key_map, value_map, block_map):
        kvs = {}
        for block_id, key_block in key_map.items():
            value_block = find_value_block(key_block, value_map)
            key = get_text(key_block, block_map)
            val = get_text(value_block, block_map)
            kvs[key] = val
        return kvs

    def find_value_block(key_block, value_map):
        for relationship in key_block['Relationships']:
            if relationship['Type'] == 'VALUE':
                for value_id in relationship['Ids']:
                    value_block = value_map[value_id]
        return value_block

    def get_text(result, blocks_map):
        text = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
        return text

    def search_value(kvs, search_key):
        for key, value in kvs.items():
            if re.search(search_key, key, re.IGNORECASE):
                return value

    # MAIN PROGRAM
    key_map, value_map, block_map = get_kv_map(image_file)

    # Get Key Value relationship
    # res_list = []
    kvs = get_kv_relationship(key_map, value_map, block_map)
    # res_list.append(kvs)

    return kvs


def row_text(image_file):
    """

    :return:
    """
    text3 = ""
    with open(image_file, 'rb') as document:
        imagebytes = bytearray(document.read())

        # Amazon Textract client
    textract = boto3.client('textract')
    # Call Amazon Textract
    response = textract.detect_document_text(Document={'Bytes': imagebytes})
    for item in response["Blocks"]:
        if item["BlockType"] == "LINE":
            text3 += item["Text"] + ','
            text3 += '\n'

    return text3


def get_key_info(text, label_key):
    info = []
    for i in labels:
        if next(iter(i)) == label_key:
            for k in i[label_key]:
                pattern = '{}.+'.format(str(k))
                pat_gen = re.finditer(pattern, text)
                for txt in pat_gen:
                    match_txt1 = txt.group()
                    info.append(match_txt1)
    return info


# get grayscale image
def get_grayscale(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


# noise removal
def remove_noise(image):
    im = cv2.imread(image)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    im = cv2.medianBlur(im, 5)
    return im


# thresholding
def thresholding(image):
    return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def resize(image):
    scale_percent = 140  # percent of original size
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    gray_img = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
    return gray_img


def clean_dict(aa):
    for i in aa:
        if i['extraction'] is not None:
            if i['label'] == 'Patient_Name':
                a = ''.join(e for e in i['extraction'] if e.isalpha() or e == ' ')
                i['extraction'] = a
            elif i['label'] == 'Policy_Certificate':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ')
                i['extraction'] = a
            elif i['label'] == 'Patient_Acct_No':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ')
                i['extraction'] = a
            elif i['label'] == 'Check_No':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ' or e == '.')
                i['extraction'] = a
            elif i['label'] == 'Patient_Mem_ID':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ')
                i['extraction'] = a
            elif i['label'] == 'Claim_Total':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ' or e == '.')
                i['extraction'] = a
            elif i['label'] == 'Provider_NPI':
                a = ''.join(e for e in i['extraction'] if e.isalnum() or e == ' ')
                i['extraction'] = a
            elif i['label'] == 'Provider_Name':
                a = ''.join(e for e in i['extraction'] if e.isalpha() or e == ' ')
                i['extraction'] = a

    aa = [i for i in aa if not (i['extraction'] is None)]
    return aa


def atena_2_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map)
            # csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    patient_name_list = []

    def generate_table_csv(table_result, blocks_map):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        remove_cell_keyword_aetna_2 = ['name',
                                       'patient name',
                                       'npi',
                                       'claim',
                                       'provider',
                                       'account',
                                       'policy']
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = text.strip()

                    for i in remove_cell_keyword_aetna_2:
                        if i in text.lower():
                            patient_name_list.append(text)
                            text = ''
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv, patient_name_list


def aetna_2_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    # lst_val = df["Service Dates"].iloc[-1]
    last_index = df.last_valid_index() + 1
    index_of_table = check('Service Dates', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]

    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['Service Dates'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = df['Service Code'].tolist() or pd.Series([None])
            new_df['PROC'] = df['PL'].tolist() or pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = df['Num Svcs'].tolist() or pd.Series([None])
            new_df['BILLED/Charged'] = df['Submitted Charges'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['Allowable / Negotiated Amount'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['Deductible'].tolist() or pd.Series([None])
            new_df['COPAY'] = df['Copay'].tolist() or pd.Series([None])
            new_df['COINS'] = pd.Series([None])
            new_df['Amount Paid'] = pd.Series([None])
            new_df['Not Payable'] = df['Not Payable'].tolist() or pd.Series([None])
            new_df['Payable Amt'] = df['Payable Amt'].tolist() or pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = df['Patient Resp'].tolist() or pd.Series([None])
            new_df['See Remarks'] = df['See Remarks'].tolist() or pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'Service Dates']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def atena_3_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']
        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map)
            # csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    patient_name_list = []

    def generate_table_csv(table_result, blocks_map):
        rows = get_rows_columns_map(table_result, blocks_map)
        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    if 'Patient Name' in text \
                            or 'name' in text \
                            or 'Patient' in text \
                            or 'Provider NPI' in text \
                            or 'Claim' in text or 'Policy' in text \
                            or 'Patient Account' in text:
                        patient_name_list.append(text)
                        break
                    csv += '{}'.format(text) + ","
                csv += '\n'
        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv, patient_name_list


def aetna_3_table_df(csv_path, pat_info_table_index):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    # lst_val = df["DATES OF SERVICE FROM THROUGH"].iloc[-1]
    last_index = df.last_valid_index() + 1
    index_of_table = check('DATES OF SERVICE FROM THROUGH', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    if len(list_of_dfs) == len(pat_info_table_index):
        if len(list_of_dfs) == 5:
            list_of_dfs[0], \
            list_of_dfs[1], list_of_dfs[2], \
            list_of_dfs[3], list_of_dfs[4] = list_of_dfs[pat_info_table_index[0]], \
                                             list_of_dfs[pat_info_table_index[1]], \
                                             list_of_dfs[pat_info_table_index[2]], \
                                             list_of_dfs[pat_info_table_index[3]], \
                                             list_of_dfs[pat_info_table_index[4]]
        if len(list_of_dfs) == 4:
            list_of_dfs[0], \
            list_of_dfs[1], list_of_dfs[2], \
            list_of_dfs[3] = list_of_dfs[pat_info_table_index[0]], \
                             list_of_dfs[pat_info_table_index[1]], \
                             list_of_dfs[pat_info_table_index[2]], \
                             list_of_dfs[pat_info_table_index[3]]
        if len(list_of_dfs) == 3:
            list_of_dfs[0], \
            list_of_dfs[1], list_of_dfs[2] = list_of_dfs[pat_info_table_index[0]], \
                                             list_of_dfs[pat_info_table_index[1]], \
                                             list_of_dfs[pat_info_table_index[2]]
        if len(list_of_dfs) == 2:
            list_of_dfs[0], \
            list_of_dfs[1] = list_of_dfs[pat_info_table_index[0]], \
                             list_of_dfs[pat_info_table_index[1]]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['DATES OF SERVICE FROM THROUGH'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = pd.Series([None])
            new_df['PROC'] = df['PROCEDURE CODE'].tolist() or pd.Series([None])
            new_df['MODS'] = df['MEDICARE APPROVED'].tolist() or pd.Series([None])
            new_df['No Of Unit/Quantity'] = pd.Series([None])
            new_df['BILLED/Charged'] = pd.Series([None])
            new_df['ALLOWED'] = df['ACTUAL EXPENSE'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['DEDUCTIBLE AMOUNT'].tolist() or pd.Series([None])
            new_df['COPAY'] = df['CO-PAY AMOUNT'].tolist() or pd.Series([None])
            new_df['COINS'] = pd.Series([None])
            new_df['Amount Paid'] = df['AMOUNT PAID'].tolist() or pd.Series([None])
            new_df['Not Payable'] = pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = pd.Series([None])
            new_df['See Remarks'] = df['SEE REMARK'].tolist() or pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'DATES OF SERVICE FROM THROUGH']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def aetna_1_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    if 'NAME' in text or 'ACNT' in text or 'SOPHAL' in text or \
                            'Policy' in text or 'Patient Account' in text:
                        break
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def aetna_1_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df = df.replace(np.nan, '', regex=True)

    last_index = df.last_valid_index() + 1
    index_of_table = check('SERV DATE', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            identified_columns = get_avialable_columns(df)
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['SERV DATE'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = pd.Series([None])
            new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = df['NOS REV MODS'].tolist() or pd.Series([None])
            new_df['No Of Unit/Quantity'] = df['TOB'].tolist() or pd.Series([None])
            new_df['BILLED/Charged'] = df['BILLED'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['ALLOWED'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['DEDUCT'].tolist() or pd.Series([None])
            new_df['COPAY'] = pd.Series([None])
            new_df['COINS'] = df['COINS'].tolist() or pd.Series([None])
            new_df['Amount Paid'] = pd.Series([None])
            new_df['Not Payable'] = pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            if 'RC_AMT' in identified_columns:
                new_df['RC_AMT'] = df[identified_columns['RC_AMT']].tolist() or pd.Series([None])
            else:
                new_df['RC_AMT'] = pd.Series([None])

            if 'GRP' in identified_columns:
                new_df['GRP'] = df[identified_columns['GRP']].tolist() or pd.Series([None])
            else:
                new_df['GRP'] = pd.Series([None])

            new_df['PROV_PD'] = df['PROV PD'].tolist() or pd.Series([None])
            new_df['Patient_responsibility'] = pd.Series([None])
            new_df['See Remarks'] = pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'SERV DATE']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def check(val, df):
    a = df.index[df[val].astype(str).str.contains(val)]
    if a.empty:
        return 'not found'
    else:
        return a.tolist()


def aetna_4_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(file_name):

        with open(file_name, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def aetna_4_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('Date of Service', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            new_df = pd.DataFrame()
            new_df["Provider's Name"] = df["Provider's Name"].tolist() or pd.Series([None])
            new_df['SERV_DATE'] = df['Date of Service'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = pd.Series([None])
            new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = pd.Series([None])
            new_df['BILLED/Charged'] = df['Billed Amount'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['Approved Amount'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = pd.Series([None])
            new_df['COPAY'] = pd.Series([None])
            new_df['COINS'] = pd.Series([None])
            new_df['Amount Paid'] = df['Amount Paid'].tolist() or pd.Series([None])
            new_df['Not Payable'] = pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = df["Patient's Responsibility"].tolist() or pd.Series([None])
            new_df['See Remarks'] = pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'Date of Service']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def cigna_3_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(file_name):

        with open(file_name, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def cigna_3_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df.columns = map(str.lower, df.columns)
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('treatment date / type', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            new_df = pd.DataFrame()
            dos_l = []
            proc_l = []
            for i in df['treatment date / type'].tolist():
                b = re.search('([0-9]{1,2}\s[a-zA-Z]+\s[0-9]{1,4})', i)
                if b is not None:
                    dos_l.append(b.group())
                else:
                    dos_l.append(i)
                c = re.search('(CPT\s[0-9]{1,6})', i)
                if c is not None:
                    proc_l.append(c.group())
                else:
                    proc_l.append('')
            new_df['SERV_DATE'] = dos_l or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = pd.Series([None])
            new_df['PROC'] = proc_l or pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = pd.Series([None])
            new_df['BILLED/Charged'] = df['billed amount'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = pd.Series([None])
            new_df['DEDUCTIBLE'] = df['discount amount'].tolist() or pd.Series([None])
            new_df['COPAY'] = pd.Series([None])
            new_df['COINS'] = pd.Series([None])
            new_df['Amount Paid'] = df['paid amount by cigna'].tolist() or pd.Series([None])
            new_df['Not Payable'] = pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = df["customer responsibility"].tolist() or pd.Series([None])
            new_df['See Remarks'] = pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'treatment date / type']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def cigna_2_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def cigna_2_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df.columns = map(str.lower, df.columns)
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('dates of service', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['dates of service'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = df['service description'].tolist() or pd.Series([None])
            new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = pd.Series([None])
            new_df['BILLED/Charged'] = df['total charge'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['ellgible expenses'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['deduct. applied'].tolist() or pd.Series([None])
            new_df['COPAY'] = df['co-pay amount'].tolist() or pd.Series([None])
            new_df['COINS'] = df['coins. applied'].tolist() or pd.Series([None])
            new_df['Amount Paid'] = df['plan payment'].tolist() or pd.Series([None])
            new_df['Not Payable'] = df['not covered'].tolist() or pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = pd.Series([None])
            new_df['See Remarks'] = df["remark code"].tolist() or pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'dates of service']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def cigna_4_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def cigna_4_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    # df.columns = map(str.lower, df.columns)
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('Service dates', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            df.columns = map(str.lower, df.columns)
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['service dates'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = df['type of service'].tolist() or pd.Series([None])
            new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = pd.Series([None])
            new_df['BILLED/Charged'] = df['amount billed'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['allowed amount'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['deductible'].tolist() or pd.Series([None])
            new_df['COPAY'] = df['copay'].tolist() or pd.Series([None])
            new_df['COINS'] = df['coinsurance*'].tolist() or pd.Series([None])
            new_df['Amount Paid'] = df['what your plan paid'].tolist() or pd.Series([None])
            new_df['Not Payable'] = df['amount not covered'].tolist() or pd.Series([None])
            new_df['Payable Amt'] = df['% paid'].tolist() or pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            new_df['GRP_RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = pd.Series([None])
            new_df['See Remarks'] = df["see notes"].tolist() or pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'Service dates']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def uhc_1_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(filename):

        with open(filename, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv_1 = generate_table_csv(table, blocks_map, index + 1)
            if 'Identifier' in csv_1:
                csv += ''
            else:
                csv += csv_1
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    if 'NAME' in text or 'ACNT' in text or 'GRP/POL' in text \
                            or 'Policy' in text \
                            or 'Patient Account' in text \
                            or 'MRN' in text:
                        break
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def uhc_1_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('SERV DATE', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            identified_columns = get_avialable_columns(df)
            new_df = pd.DataFrame()
            if 'SERV_DATE' in identified_columns:
                serv_date_list = df[identified_columns['SERV_DATE']].tolist()

                date_list = []
                for i in serv_date_list:
                    a = re.search('(\s\d\d\d\d\d\d)', i)
                    if a is not None:
                        date_list.append(a.group())
                    else:
                        date_list.append(i)
                if date_list:
                    serv_date_list1 = date_list
                else:
                    serv_date_list1 = serv_date_list
                serv_date_list1 = [j.replace('.0', '') for j in serv_date_list1]
                new_df['SERV_DATE'] = serv_date_list1 or pd.Series([None])
            else:
                new_df['SERV_DATE'] = pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            if 'POS' in identified_columns:
                new_df['POS'] = df[identified_columns['POS']].tolist()
            else:
                new_df['POS'] = pd.Series([None])

            if 'PROC' in identified_columns:
                new_df['PROC'] = df[identified_columns['PROC']].tolist()
            else:
                new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = df['MODS'].tolist() or pd.Series([None])
            if 'No Of Unit/Quantity' in identified_columns:
                new_df['No Of Unit/Quantity'] = df[identified_columns['No Of Unit/Quantity']].tolist()
            else:
                new_df['No Of Unit/Quantity'] = pd.Series([None])

            new_df['BILLED/Charged'] = df['BILLED'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['ALLOWED'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['DEDUCT'].tolist() or pd.Series([None])
            new_df['COPAY'] = pd.Series([None])
            new_df['COINS'] = df['COINS'].tolist() or pd.Series([None])
            new_df['Amount Paid'] = pd.Series([None])
            new_df['Not Payable'] = pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            if 'GRP' in identified_columns:
                new_df['GRP'] = df[identified_columns['GRP']].tolist()
            else:
                new_df['GRP'] = pd.Series([None])
            if 'RC_AMT' in identified_columns:
                new_df['RC_AMT'] = df[identified_columns['RC_AMT']].tolist()
            else:
                new_df['RC_AMT'] = pd.Series([None])
            new_df['PROV_PD'] = df['PROV PD'].tolist() or pd.Series([None])
            new_df['Patient_responsibility'] = pd.Series([None])
            new_df['See Remarks'] = pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'SERV DATE']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


def uhc_5_table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(black)
        else:
            newPixels.append(white)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        text1 = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text1 = text.replace(',', '')
        return text1

    def get_table_csv_results(file_name):

        with open(file_name, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n\n\n\n'
            # break
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        # table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''
        # column_name = ['DATE(S) OF SERVICE', 'DESCRIPTION OF SERVICE', 'UNITS',
        #                'BILLED AMT', 'ALLOWED AMT', 'DEDUCT AMT',
        #                'COPAY/COINS AMT', 'COPAY/COINS AMT',
        #                'PAID TO PROVIDER AMT', 'DISALLOW AMT',
        #                'GRP CD/ RSN CD', 'PATIENT RESP AMT', 'RMK CD']
        for row_index, cols in rows.items():
            if len(cols.items()) > 5:
                for col_index, text in cols.items():
                    text = re.sub(' +', ' ', text)
                    text = text.strip()
                    if 'NAME' in text or 'CARRIER' in text or 'PROV NPI:' in text or 'Policy' in text or \
                            'SUBSCRIBER' in text or 'MRN' in text or \
                            'MEMBER' in text:
                        break
                    csv += '{}'.format(text) + ","
                csv += '\n'

        return csv

    table_csv = get_table_csv_results(file_name)
    if table_csv == "<b> NO Table FOUND </b>":
        table_csv = "NO Table FOUND"
    else:
        output_file = 'output.csv'
        # replace content
        with open(path + '/' + output_file, "a") as fout:
            fout.write(table_csv)
    return table_csv


def uhc_5_table_df(csv_path):
    df = pd.read_csv(csv_path)
    df.columns = df.columns.str.strip()
    df = df.replace(np.nan, '', regex=True)
    last_index = df.last_valid_index() + 1
    index_of_table = check('DATE(S) OF SERVICE', df)
    if index_of_table == 'not found':
        index_of_table = [0]
    index_of_table.append(last_index)
    l_mod = [0] + index_of_table + [max(index_of_table) + 1]

    list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
    list_of_dfs = [i for i in list_of_dfs if not i.empty]
    list_of_new_dfs = []
    for df in list_of_dfs:
        if not df.empty:
            df.columns = df.columns.str.strip()
            identified_columns = get_avialable_columns(df)
            new_df = pd.DataFrame()
            new_df['SERV_DATE'] = df['DATE(S) OF SERVICE'].tolist() or pd.Series([None])
            new_df['SERV_DATE_FROM'] = pd.Series([None])
            new_df['SERV_DATE_TO'] = pd.Series([None])
            new_df['POS'] = df['DESCRIPTION OF SERVICE'].tolist() or pd.Series([None])
            new_df['PROC'] = pd.Series([None])
            new_df['MODS'] = pd.Series([None])
            new_df['No Of Unit/Quantity'] = df['UNITS'].tolist() or pd.Series([None])
            new_df['BILLED/Charged'] = df['BILLED AMT'].tolist() or pd.Series([None])
            new_df['ALLOWED'] = df['ALLOWED AMT'].tolist() or pd.Series([None])
            new_df['DEDUCTIBLE'] = df['DEDUCT AMT'].tolist() or pd.Series([None])
            new_df['COPAY'] = df['COPAY/COINS AMT'].tolist() or pd.Series([None])
            new_df['COINS'] = df['COPAY/COINS AMT'].tolist() or pd.Series([None])
            new_df['Amount Paid'] = df['PAID TO PROVIDER AMT'].tolist() or pd.Series([None])
            new_df['Not Payable'] = df['DISALLOW AMT'].tolist() or pd.Series([None])
            new_df['Payable Amt'] = pd.Series([None])
            new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
            if 'RC_AMT' in identified_columns:
                new_df['RC_AMT'] = df[identified_columns['RC_AMT']].tolist() or pd.Series([None])
            else:
                new_df['RC_AMT'] = pd.Series([None])

            if 'GRP' in identified_columns:
                new_df['GRP'] = df[identified_columns['GRP']].tolist() or pd.Series([None])
            else:
                new_df['GRP'] = pd.Series([None])
            new_df['PROV_PD'] = pd.Series([None])
            new_df['Patient_responsibility'] = df['PATIENT RESP AMT'].tolist() or pd.Series([None])
            new_df['See Remarks'] = df['RMK CD'].tolist() or pd.Series([None])
            new_df = new_df.dropna(how='all')
            new_df = new_df[new_df.SERV_DATE != 'DATE(S) OF SERVICE']
            if not new_df.empty:
                list_of_new_dfs.append(new_df)

    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    # if os.path.exists(path + '/output.csv'):
    #     os.remove(path + '/output.csv')
    # for df in list_of_new_dfs:
    #     with open(path + '/output.csv', 'a') as f:
    #         df.to_csv(f, index=False)
    #         f.write("\n\n\n")
    return table_json, list_of_new_dfs


map_dict = {
    'SERV_DATE': ['Service Dates', 'DATES OF SERVICE FROM THROUGH', 'SERV DATE',
                  'Date of Service', 'treatment date / type',
                  'dates of service', 'Service dates',
                  'DATE(S) OF SERVICE', 'DATE', 'Service Date',
                  'Dates of Service', 'DATE OF SERVICE FROM TO',
                  'FROM / TO DATES', 'Dase of Service', 'SERVICE DATE(S)',
                  'SERVICE DATES FROM/TO', 'Service Date(s)'],
    'SERV_DATE_FROM': [],
    'SERV_DATE_TO': [],
    'POS': ['Service Code', 'service description',
            'type of service', 'DESCRIPTION OF SERVICE',
            'POS', 'POS NOS', 'Codeor Description',
            'PROC DESCRIPTION LINE CODE OF SERVICE'],
    'PROC': ['PL', 'PROCEDURE CODE', 'PROC', 'PROC MODS', 'Proc. Code', 'PROC CODE',
             'Proc/Rev DRG Code', 'Prue Kev DRG Code', 'Proc', 'Proc/Rev',
             'Proc/Rev DRC Code', 'PROCEDURE CODE CVD/NCVD', 'Service Code',
             'Rend Prov ID', 'Sub Proc / Modifier / Units'],
    'MODS': ['MEDICARE APPROVED', 'NOS REV MODS', 'MODS', 'PROC MODS',
             'Mod', 'Mods', 'R levenue Code'],
    'No Of Unit/Quantity': ['Num Svcs', 'TOB', 'NOS', 'UNITS', 'POS NOS', 'Qty',
                            'Qty Clanzed Anount', 'Unit/ Time/ Miles',
                            'Units', 'Adjud Proc / Modifier / Units'],
    'BILLED/Charged': ['Submitted Charges', 'BILLED', 'Billed Amount',
                       'billed amount', 'total charge', 'amount billed',
                       'BILLED AMT', 'Amount Charged', 'Amount Billed',
                       'CHARGES SUBMITTED', 'AMOUNT BILLED', 'Charged Amount',
                       'Charged', 'Billed', 'TOTAL CHARGES', 'Billed',
                       'Charge/Adjustments'],
    'ALLOWED': ['Allowable / Negotiated Amount', 'ACTUAL EXPENSE',
                'ALLOWED', 'Approved Amount', 'ellgible expenses',
                'allowed amount', 'ALLOWED AMT', 'Allowed Amount', 'DISCOUNT',
                'ALLOWABLE AMOUNT', 'Allowed Amoust',
                'Allowed', 'Discount', 'ALLOWED AMOUNT'],
    'DEDUCTIBLE': ['Deductible', 'DEDUCTIBLE AMOUNT',
                   'DEDUCT', 'discount amount', 'deduct. applied',
                   'deductible', 'DEDUCT AMT', 'Deductible Amount',
                   'Discount Amount', 'DEDUCT APPLIED',
                   'DEDUCTIONS/OTHER INELIGIBLE',
                   'Deductible', 'OTHER INSURANCE DOLLARS'],
    'COPAY': ['Copay', 'CO-PAY AMOUNT', 'co-pay amount',
              'copay', 'COPAY/COINS AMT', 'COB Allowed',
              'Co-pay Amount', 'COPAY', 'Cn Pny', 'Ce Pay'],
    'COINS': ['COINS', 'coins. applied', 'coinsurance*',
              'COPAY/COINS AMT', 'Coins Amount',
              'Coinsurance', 'Coins', 'Colas'],
    'Amount Paid': ['AMOUNT PAID', 'Amount Paid',
                    'paid amount by cigna', 'plan payment',
                    'what your plan paid', 'PAID TO PROVIDER AMT',
                    'Amount Paid', 'Covered Amount', 'AMOUNT PAID',
                    'Amout Paid', 'Pald', 'Paid', 'Supp Info (AMT)'],
    'Not Payable': ['Not Payable', 'not covered', 'amount not covered',
                    'DISALLOW AMT', 'Not Covered', 'Inaligible',
                    'Not Covered', 'NON-PAID AMOUNT','SERVICES NOT COVERED',
                    'ineligible', 'OTHER AMOUNTS NOT COVERED', 'Ineligible'],
    'Payable Amt': ['Payable Amt', '% paid', 'Net Payment Amount',
                    'Payment Amount', 'BENEFITS PAYABLE', 'Seq Amt', 'Payment'],
    'Denial_Code/Explanation Codes/Adjustment_Reason': ['Other Adjustment',
                                                        'Adj Den', 'Ady Dem', 'Codes'],
    'GRP': ['Unnamed: 9', 'GRP', 'GRP CD', 'Unnamed: 10', 'Unnamed: 11', 'Unnamed: 8'],
    'RC_AMT': ['Unnamed: 9', 'GRP/RC-AMT', 'GRP CD/ RSN CD'],
    'PROV_PD': ['PROV PD'],
    'Patient_responsibility': ['Patient Resp', "Patient's Responsibility",
                               'customer responsibility', 'PATIENT RESP AMT'],
    'See Remarks': ['See Remarks', 'SEE REMARK',
                    'remark code', 'see notes', 'RMK CD',
                    'Reason Coda(s)', 'Rmk Code', 'Descriptiou',
                    'Desertption', 'Description',
                    'Rev', 'HCPCS', 'Status', 'Remark/Payer Code']
}


def get_avialable_columns(df):
    identified_columns = {}
    for col in df.columns:
        for key in map_dict:
            for vl in map_dict[key]:
                if vl == col:
                    identified_columns[key] = col
    return identified_columns


def get_dos_key_for_df(df):
    dos_label_to_split_df = ''
    for i in df.columns:
        for j in map_dict['SERV_DATE']:
            if i == j:
                dos_label_to_split_df = str(i)
    return dos_label_to_split_df


def common_format_table_df(csv_path):
    last_index = ''
    list_of_new_dfs = []
    df = pd.read_csv(csv_path, error_bad_lines=False)
    df.columns = df.columns.str.strip()
    df = df.replace(np.nan, '', regex=True)
    df = df.astype(str)
    try:
        dos_label_to_split_df = get_dos_key_for_df(df)
        last_index = df.last_valid_index() + 1
        index_of_table = check(dos_label_to_split_df, df)
        if index_of_table == 'not found':
            index_of_table = [0]
        index_of_table.append(last_index)

        l_mod = [0] + index_of_table + [max(index_of_table) + 1]
        list_of_dfs = [df.iloc[l_mod[n]:l_mod[n + 1]] for n in range(len(l_mod) - 1)]
        list_of_dfs = [i for i in list_of_dfs if not i.empty]
        for df in list_of_dfs:
            if not df.empty:
                df.columns = df.columns.str.strip()
                identified_columns = get_avialable_columns(df)
                new_df = pd.DataFrame()
                if 'SERV_DATE' in identified_columns:
                    serv_date_list = df[identified_columns['SERV_DATE']].tolist()
                    date_list = []
                    for i in serv_date_list:
                        a = re.search('(\s\d\d\d\d\d\d)', i)
                        if a is not None:
                            date_list.append(a.group())
                        else:
                            date_list.append(i)
                    if date_list:
                        serv_date_list1 = date_list
                    else:
                        serv_date_list1 = serv_date_list
                    serv_date_list1 = [j.replace('.0', '') for j in serv_date_list1]
                    new_df['SERV_DATE'] = serv_date_list1 or pd.Series([None])
                else:
                    new_df['SERV_DATE'] = pd.Series([None])

                new_df['SERV_DATE_FROM'] = pd.Series([None])
                new_df['SERV_DATE_TO'] = pd.Series([None])

                if 'POS' in identified_columns:
                    POS_list = df[identified_columns['POS']].tolist()
                    pos_l = []
                    for ps in POS_list:
                        b = re.search('(\d+\s)', ps)
                        if b is not None:
                            pos_l.append(b.group())
                        else:
                            pos_l.append(ps)
                    new_df['POS'] = pos_l or pd.Series([None])
                else:
                    new_df['POS'] = pd.Series([None])

                if 'PROC' in identified_columns:
                    PROC_l = [j.replace('.0', '') for j in df[identified_columns['PROC']].tolist()]
                    proc_l = []
                    for ps in PROC_l:
                        b = re.search('(\s\d+)', ps)
                        if b is not None:
                            proc_l.append(b.group())
                        else:
                            proc_l.append(ps)
                    new_df['PROC'] = proc_l or pd.Series([None])
                else:
                    new_df['PROC'] = pd.Series([None])
                if 'MODS' in identified_columns:
                    MODS_list = df[identified_columns['MODS']].tolist()
                    mods_l = []
                    for ps in MODS_list:
                        if not 'M15' in ps:
                            b = re.search('([M]\d+)', ps)
                            if b is not None:
                                mods_l.append(b.group())
                            else:
                                mods_l.append(ps)
                        else:
                            mods_l.append('')
                    new_df['MODS'] = mods_l or pd.Series([None])
                else:
                    new_df['MODS'] = pd.Series([None])

                if 'No Of Unit/Quantity' in identified_columns:
                    NOS_list = df[identified_columns['No Of Unit/Quantity']].tolist()
                    nos_l = []
                    for ps in NOS_list:
                        b = re.search('(\s\d+)', ps)
                        if b is not None:
                            nos_l.append(b.group())
                        else:
                            nos_l.append(ps)
                    new_df['No Of Unit/Quantity'] = nos_l or pd.Series([None])
                else:
                    new_df['No Of Unit/Quantity'] = pd.Series([None])

                if 'BILLED/Charged' in identified_columns:
                    new_df['BILLED/Charged'] = df[identified_columns['BILLED/Charged']].tolist() or pd.Series([None])
                else:
                    new_df['BILLED/Charged'] = pd.Series([None])

                if 'ALLOWED' in identified_columns:
                    new_df['ALLOWED'] = df[identified_columns['ALLOWED']].tolist() or pd.Series([None])
                else:
                    new_df['ALLOWED'] = pd.Series([None])

                if 'DEDUCTIBLE' in identified_columns:
                    new_df['DEDUCTIBLE'] = df[identified_columns['DEDUCTIBLE']].tolist() or pd.Series([None])
                else:
                    new_df['DEDUCTIBLE'] = pd.Series([None])

                if 'COPAY' in identified_columns:
                    new_df['COPAY'] = df[identified_columns['COPAY']].tolist() or pd.Series([None])
                else:
                    new_df['COPAY'] = pd.Series([None])

                if 'COINS' in identified_columns:
                    new_df['COINS'] = df[identified_columns['COINS']].tolist() or pd.Series([None])
                else:
                    new_df['COINS'] = pd.Series([None])

                if 'Amount Paid' in identified_columns:
                    new_df['Amount Paid'] = df[identified_columns['Amount Paid']].tolist() or pd.Series([None])
                else:
                    new_df['Amount Paid'] = pd.Series([None])

                if 'Not Payable' in identified_columns:
                    new_df['Not Payable'] = df[identified_columns['Not Payable']].tolist() or pd.Series([None])
                else:
                    new_df['Not Payable'] = pd.Series([None])

                if 'Payable Amt' in identified_columns:
                    new_df['Payable Amt'] = df[identified_columns['Not Payable']].tolist() or pd.Series([None])
                else:
                    new_df['Payable Amt'] = pd.Series([None])

                if 'Denial_Code/Explanation Codes/Adjustment_Reason' in identified_columns:
                    new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = df[identified_columns[
                        'Denial_Code/Explanation Codes/Adjustment_Reason']].tolist() or pd.Series([None])
                else:
                    new_df['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])

                if 'RC_AMT' in identified_columns:
                    new_df['RC_AMT'] = df[identified_columns['RC_AMT']].tolist() or pd.Series([None])
                else:
                    new_df['RC_AMT'] = pd.Series([None])

                if 'GRP' in identified_columns:
                    new_df['GRP'] = df[identified_columns['GRP']].tolist() or pd.Series([None])
                else:
                    new_df['GRP'] = pd.Series([None])

                if 'PROV_PD' in identified_columns:
                    new_df['PROV_PD'] = df[identified_columns['PROV_PD']].tolist() or pd.Series([None])
                else:
                    new_df['PROV_PD'] = pd.Series([None])

                if 'Patient_responsibility' in identified_columns:
                    new_df['Patient_responsibility'] = df[identified_columns[
                        'Patient_responsibility']].tolist() or pd.Series([None])
                else:
                    new_df['Patient_responsibility'] = pd.Series([None])

                if 'See Remarks' in identified_columns:
                    new_df['See Remarks'] = df[identified_columns['See Remarks']].tolist() or pd.Series([None])
                else:
                    new_df['See Remarks'] = pd.Series([None])

                new_df = new_df.dropna(how='all')
                new_df = new_df[new_df.SERV_DATE != dos_label_to_split_df]
                if not new_df.empty:
                    list_of_new_dfs.append(new_df)
    except Exception as e:
        print(e)
    if not list_of_new_dfs:
        list_of_new_dfs = [df]
    df22 = pd.concat(list_of_new_dfs, axis=1, keys=np.arange(len(list_of_new_dfs)))
    df22.columns = ['{}{}'.format(b, a) for a, b in df22.columns]
    table_json = df22.to_json(orient="records")

    return table_json, list_of_new_dfs




# def table_textract(image_file):
#     """
#
#     :return:
#     """
#     img = Image.open(image_file).convert("LA")
#
#     pixels = img.getdata()
#
#     newPixels = []
#
#     # Compare each pixel
#     for pixel in pixels:
#         if pixel < threshold:
#             newPixels.append(black)
#         else:
#             newPixels.append(white)
#
#     # Create and save new image.
#     newImg = Image.new("RGB", img.size)
#     newImg.putdata(newPixels)
#     newImg.save("newImage.png")
#
#     adjust_sharpness('newImage.png',
#                      'new_sharp_image.png',
#                      1.7)
#
#     file_name = 'new_sharp_image.png'
#
#     # get the results
#     client = boto3.client(
#         service_name='textract',
#         region_name='us-east-1',
#         endpoint_url='https://textract.us-east-1.amazonaws.com',
#     )
#
#     def get_rows_columns_map(table_result, blocks_map):
#         rows = {}
#         for relationship in table_result['Relationships']:
#             if relationship['Type'] == 'CHILD':
#                 for child_id in relationship['Ids']:
#                     cell = blocks_map[child_id]
#                     if cell['BlockType'] == 'CELL':
#                         row_index = cell['RowIndex']
#                         col_index = cell['ColumnIndex']
#                         if row_index not in rows:
#                             # create new row
#                             rows[row_index] = {}
#
#                         # get the text value
#                         rows[row_index][col_index] = get_text(cell, blocks_map)
#         return rows
#
#     def get_text(result, blocks_map):
#         text = ''
#         text1 = ''
#         if 'Relationships' in result:
#             for relationship in result['Relationships']:
#                 if relationship['Type'] == 'CHILD':
#                     for child_id in relationship['Ids']:
#                         word = blocks_map[child_id]
#                         if word['BlockType'] == 'WORD':
#                             text += word['Text'] + ' '
#                             text1 = str(text.replace(',', ''))
#         return text1
#
#     def get_table_csv_results(filename):
#
#         with open(filename, 'rb') as file:
#             img_test = file.read()
#             bytes_test = bytearray(img_test)
#
#         # process using image bytes
#         response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])
#
#         # Get the text blocks
#         blocks = response['Blocks']
#
#         blocks_map = {}
#         table_blocks = []
#         for block in blocks:
#             blocks_map[block['Id']] = block
#             if block['BlockType'] == "TABLE":
#                 table_blocks.append(block)
#
#         if len(table_blocks) <= 0:
#             return "<b> NO Table FOUND </b>"
#
#         csv = ''
#         for index, table in enumerate(table_blocks):
#             csv_1 = generate_table_csv(table, blocks_map)
#             # if 'Identifier' in csv_1:
#             #     csv += ''
#             # else:
#             csv += csv_1
#             csv += '\n\n\n\n\n'
#             # break
#             # csv += generate_table_csv(table, blocks_map, index +1)
#         return csv
#
#     def generate_table_csv(table_result, blocks_map):
#         rows = get_rows_columns_map(table_result, blocks_map)
#
#         # table_id = 'Table_' + str(table_index)
#         # if(table_index > 1):
#
#         # get cells.
#
#         # csv = 'Table: {0}\n\n'.format(table_id)
#         # print(rows.items())
#         # for i in rows.items():
#         #     for j in i[1]:
#         #         if 'Statement Summary' in i[1][j] or \
#         #                 '# OF CLAIMS 1' in i[1][j] or '# OF' in i[1][j]:
#         #             rows = {}
#
#         csv = ''
#         # remove_cell_keyword_list = ['name', 'carrier', 'prov npi:', 'policy',
#         #                             'subscriber', 'mrn', 'member', 'corrected',
#         #                             'patient', 'provider', 'tex id:', 'number',
#         #                             'items', 'line', 'claim #', 'group #:']
#         for row_index, cols in rows.items():
#                 for col_index, text in cols.items():
#                     csv += "'{}".format(str(text)) + "',"
#                 csv += '\n'
#
#         return csv
#
#     table_csv = get_table_csv_results(file_name)
#     if table_csv == "<b> NO Table FOUND </b>":
#         table_csv = "NO Table FOUND"
#     else:
#         output_file = '231803_Bajaj.csv'
#         # replace content
#         with open(path + '/' + output_file, "a") as fout:
#             fout.write(table_csv)
#     return table_csv
