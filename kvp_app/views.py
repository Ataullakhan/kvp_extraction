import io
import os
from django.shortcuts import render
import editdistance
# from kvp_app.EOB_prediction import text_extract_v1, image_processing
# from kvp_app.utils import text_extract
from kvp_app.format_utils import pdf_format
from kvp_app.models import KvpJson
from kvp_app.noteshrink import notescan_main
from kvp_app.regex_kvp_utils import find_formate, waystar_total_claim_info
# from kvp_app.table_utils import tabnet_res, extract_cell_images_from_table, df_structure
from kvp_app.text_currection import page_text_correction, merge_forms_and_regex
from kvp_app.utils import get_date_info, get_numeric_info, get_float_info, table_textract, \
    get_key_info, get_trim_info, clean_dict, atena_3_table_textract, aetna_2_table_df, atena_2_table_textract, \
    aetna_3_table_df, aetna_1_table_textract, aetna_1_table_df, aetna_4_table_df, aetna_4_table_textract, \
    cigna_3_table_textract, cigna_3_table_df, cigna_2_table_df, cigna_2_table_textract, cigna_4_table_textract, \
    cigna_4_table_df, uhc_1_table_textract, uhc_1_table_df, uhc_5_table_df, uhc_5_table_textract, textrect, \
    common_format_table_df, get_numeric_info_uhc_1, forms_texract
from kvp_extraction import settings
from django.core.files.storage import FileSystemStorage
from pdf2image import convert_from_bytes
# import glob
import filetype
# import itertools
import cv2
import numpy as np
import skimage.filters as filters
import matplotlib.pyplot as plt
import re
from PIL import Image
# import numpy as np
import pytesseract
import argparse
# import ast
import json
import pandas as pd
from itertools import zip_longest
import time

path = settings.MEDIA_ROOT


# def home(request):
#     """
#     :param request:
#     :return:
#     """
#     # try:
#     table_json = []
#     client_name = ''
#     page_format = ''
#     final_dfs = []
#     text = ''
#     forms_kvp = []
#     p_n = ''
#     c_n = ''
#     p_a_n = ''
#     # config = '--psm 6 -c tessedit_char_blacklist=`~!@%^&*()£_-“+={[}}|;‘©,<>?”'
#     csv_file = 'output.csv'
#     html_df = []
#     list_of_new_dfs = []
#     if os.path.exists(path + '/output.csv'):
#         os.remove(path + '/output.csv')
#     # filelist = glob.glob(os.path.join(path + "/crop_image/", "*.*"))
#     # for f in filelist:
#     #     os.remove(f)
#     if request.method == 'POST' and request.FILES['files']:
#         myfile = request.FILES['files']
#         fs = FileSystemStorage()
#         filename = fs.save(myfile.name, myfile)
#         files = path + '/' + filename
#         file_type = filetype.guess(files)
#         # image_or_file = ''
#         pat_info_list = []
#         if file_type.extension == 'pdf':
#             # with open(files, 'rb') as pdf:
#             pages3 = convert_from_bytes(open(files, 'rb').read())
#             # start1 = time.time()
#             # page_format , pages3_1 = pdf_format(files)
#             # end1 = time.time()
#             for idx, page in enumerate(pages3):
#                 # start2 = time.time()
#                 page.save(path + '/converted.jpg', 'jpeg')
#                 documentName = path + '/converted.jpg'
#
#                 # dict_1 = text_extract_v1(documentName, model_name)
#                 # try:
#                 #     degree = pytesseract.image_to_osd(Image.open(documentName))
#                 #     if "Rotate: 90" in degree:
#                 #         # SCALE = 20
#                 #         src = cv2.imread(documentName)
#                 #         img = cv2.rotate(src, cv2.cv2.ROTATE_90_CLOCKWISE)
#                 #         plt.imsave(path + "/Py_Rotated_image.png", img)
#                 #         rotate_image = path + "/Py_Rotated_image.png"
#                 #     elif "Rotate: 270" in degree:
#                 #         src = cv2.imread(documentName)
#                 #         img = cv2.rotate(src, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)
#                 #         plt.imsave(path + "/Py_Rotated_image.png", img)
#                 #         rotate_image = path + "/Py_Rotated_image.png"
#                 #     else:
#                 #         rotate_image = documentName
#                 # except Exception as e:
#                 # rotate_image = documentName
#                 # input_noteshrink = argparse.Namespace(basename='page',
#                 #                                       filenames=[rotate_image],
#                 #                                       global_palette=False, num_colors=8,
#                 #                                       postprocess_cmd=None, quiet=False,
#                 #                                       sample_fraction=0.05, sat_threshold=0.2,
#                 #                                       saturate=True, sort_numerically=True,
#                 #                                       value_threshold=0.25, white_bg=False)
#                 #
#                 # output_filename = notescan_main(input_noteshrink)
#                 try:
#                     # img_for_table = table_dark_img_procesing(rotate_image)
#                     # plt.imsave(path + "/table_input_image.png", img_for_table)
#                     # img_for_table1 = path + "/table_input_image.png"
#                     # if page_format == 'Aetna_2':
#                     #     atena_2_table_textract(rotate_image)
#                     #     # pat_info_list.append(pat_infoo)
#                     # elif page_format == 'Aetna_3':
#                     #     table__, pat_infoo = atena_3_table_textract(rotate_image)
#                     #     pat_info_list.append(pat_infoo)
#                     # elif page_format == 'Aetna_1':
#                     #     aetna_1_table_textract(rotate_image)
#                     # elif page_format == 'Aetna_4':
#                     #     aetna_4_table_textract(rotate_image)
#                     # elif page_format == 'CIGNA_3':
#                     #     cigna_3_table_textract(rotate_image)
#                     # elif page_format == 'CIGNA_2':
#                     #     cigna_2_table_textract(rotate_image)
#                     # elif page_format == 'CIGNA_4':
#                     #     cigna_4_table_textract(rotate_image)
#                     # elif page_format == 'UHC_1':
#                     #     uhc_1_table_textract(rotate_image)
#                     # elif page_format == 'UHC_5':
#                     #     uhc_5_table_textract(rotate_image)
#                     # else:
#                     table_textract(documentName)
#                 except Exception as e:
#                     print(e)
#                 # end2 = time.time()
#                 # print('phase2', end2-start2)
#                 try:
#                     # start3 = time.time()
#                     forms = forms_texract(documentName)
#                     forms_kvp.append(forms)
#                     text += textrect(documentName)
#                     # end3 = time.time()
#                     # print('phase3' , end3-start3)
#                 except Exception as e:
#                     print(e)
#
#                 df_lists = []
#                 final_dfs.append(df_lists)
#         # start4 = time.time()
#         text = text.replace(':\n', ': ')
#         text = page_text_correction(text)
#         result_kvp_1, patient_name_l_1, claim_id_l_1, patient_acct_no_l_1 = find_formate(text, page_format)
#         result_kvp = merge_forms_and_regex(forms_kvp, result_kvp_1)
#         # end4 = time.time()
#
#         pat_info_table_index = []
#         # GEt Table Sequence By Format
#         # if page_format == 'Aetna_3':
#         #     pat_info1 = [item for sublist in pat_info_list for item in sublist]
#         #     all_data = patient_name_l_1 + claim_id_l_1 + patient_acct_no_l_1
#         #     pat_info2 = []
#         #     for i in pat_info1:
#         #         i = i.replace('Patient Name:', ' ')
#         #         i = i.replace('Patient Account:', ' ')
#         #         i = i.replace('Patient Account:', ' ')
#         #         i = i.replace('Patient Accounta', ' ')
#         #         m = i.split()
#         #         for j in m:
#         #             for k in all_data:
#         #                 if editdistance.eval(k, j) <= 3:
#         #                     i = i.replace(j, k)
#         #         pat_info2.append(i)
#         #
#         #     index_dict = {}
#         #     count = 0
#         #     for i, j, k in zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, fillvalue=""):
#         #         index_dict[count] = [i, j, k]
#         #         count += 1
#         #     for i in pat_info2:
#         #         for j in index_dict:
#         #             for k in index_dict[j]:
#         #                 if k in i:
#         #                     if j not in pat_info_table_index:
#         #                         pat_info_table_index.append(j)
#
#         for i in result_kvp:
#             if i['extraction'] is not None:
#                 if i['extraction'] == '':
#                     val = get_key_info(text, i['label'])
#                     if val:
#                         i['extraction'] = val[0] or i['extraction']
#                 if i['label'] == 'Processed_Date' or i['label'] == 'Date':
#                     date = get_date_info(i['extraction'])
#                     if date:
#                         i['extraction'] = date[0] or i['extraction']
#                         i['extraction'] = re.sub(r'^.*?(Date|:|DATE:|Date:|DATE)', '', i['extraction'])
#                 if i['label'] == 'Check_No':
#                     check_no = get_numeric_info(i['extraction'])
#                     if check_no:
#                         i['extraction'] = check_no[0] or i['extraction']
#                         i['extraction'] = get_trim_info(i['extraction'])[0]
#                 if i['label'] == 'Check_Amount':
#                     check_amt = get_float_info(i['extraction'])
#                     if check_amt:
#                         i['extraction'] = check_amt[0] or i['extraction']
#                 # if i['label'] == 'Patient_Mem_ID':
#                 #     patient_mem_id = get_float_info(i['extraction'])
#                 #     if patient_mem_id:
#                 #         i['extraction'] = patient_mem_id[0] or i['extraction']
#                 #         i['extraction'] = get_trim_info(i['extraction'])[0]
#                 if i['label'] == 'Claim_Id':
#                     # if page_format == 'UHC_1':
#                     #     Claim_Id = get_numeric_info_uhc_1(i['extraction'])
#                     #     i['extraction'] = Claim_Id[0] or i['extraction']
#                     #     i['extraction'] = i['extraction'].replace('SACKETT', '')
#                     #     i['extraction'] = i['extraction'].replace('SACKETT', '')
#                     # else:
#                         Claim_Id = get_numeric_info(i['extraction'])
#                         if Claim_Id:
#                             i['extraction'] = Claim_Id[0] or i['extraction']
#                             i['extraction'] = i['extraction'].replace(':', '')
#                             i['extraction'] = get_trim_info(i['extraction'])[0]
#                             c_n = i['extraction']
#                 if i['label'] == 'Patient_Name':
#                     Patient_Name = re.sub('\W+', ' ', i['extraction'])
#                     Patient_Name = re.sub(r'^.*?(Name|name|NAME|PATIENT|Patient)', '', Patient_Name)
#                     Patient_Name = Patient_Name.replace('PATIENT NAME', '')
#                     Patient_Name = Patient_Name.replace('Patient', '')
#                     Patient_Name = Patient_Name.replace('atient Name', '')
#                     if Patient_Name:
#                         i['extraction'] = Patient_Name or i['extraction']
#                         p_n = i['extraction']
#                 if i['label'] == 'Patient_Acct_No':
#                     Patient_Acct_No = re.sub(r'^.*?(#:|number|ACCT|Acct|:|ACNT|Acnt)', '', i['extraction'])
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient ID', '')
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient Account', '')
#                     Patient_Acct_No = Patient_Acct_No.replace('Patient Acoounts', '')
#                     if Patient_Acct_No:
#                         i['extraction'] = Patient_Acct_No or i['extraction']
#                         p_a_n = i['extraction']
#                 if i['label'] == 'Group_No':
#                     Group_No = get_numeric_info(i['extraction'])
#                     if Group_No:
#                         i['extraction'] = Group_No[0] or i['extraction']
#                         i['extraction'] = get_trim_info(i['extraction'])[0]
#                         i['extraction'] = re.sub(r'^.*?(#:|number|:|Number:)', '', i['extraction'])
#
#                 if i['label'] == 'Provider_NPI':
#                     Provider_NPI = re.sub(r'^.*?(#:|number|NPL|NPI|:|#|PI|Provider|provider|der)', '', i['extraction'])
#                     i['extraction'] = Provider_NPI or i['extraction']
#
#                 if i['label'] == 'PT_RESP':
#                     PT_RESP = re.sub(r'^.*?(#:|RESP|PT|:#|:|#)', '', i['extraction'])
#                     i['extraction'] = PT_RESP or i['extraction']
#                 if i['label'] == 'Provider_Name':
#                     Provider_Name = re.sub('\W+', ' ', i['extraction'])
#                     if Provider_Name:
#                         i['extraction'] = Provider_Name or i['extraction']
#
#                 if i['label'] == 'Payer_Name':
#                     client_name = i['extraction']
#                     Payer_Name = re.sub('\W+', ' ', i['extraction'])
#                     Payer_Name = re.sub(r'^.*?(FROM|:|#:|TO|FROM:)', '', Payer_Name)
#                     Payer_Name = Payer_Name.replace('SUBJECT', '')
#                     if Payer_Name:
#                         i['extraction'] = Payer_Name or i['extraction']
#                         client_name = i['extraction']
#
#                 if i['label'] == 'Payee_ID':
#                     Payee_ID = re.sub('\W+', ' ', i['extraction'])
#                     Payee_ID = re.sub(r'^.*?(PAYEE ID:|:|#:|ID|PAYEE:)', '', Payee_ID)
#                     Payee_ID = Payee_ID.replace('SUBJECT', '')
#                     if Payee_ID:
#                         i['extraction'] = Payee_ID or i['extraction']
#
#         try:
#             if 'WAYSTAR' in filename:
#                 waystar_total_claim_val = waystar_total_claim_info(text)
#                 for i in waystar_total_claim_val:
#                     result_kvp.append(i)
#             # if page_format == 'UHC_1':
#             #     uhc_1_total_claim_val = waystar_total_claim_info(text)
#             #     for i in uhc_1_total_claim_val:
#             #         result_kvp.append(i)
#         except Exception as e:
#             print(e)
#         result_kvp = [dict(t) for t in {tuple(d.items()) for d in result_kvp}]
#
#     #     dict_ls = []
#     #     for i in final_dfs:
#     #         i = i.replace(np.nan, '', regex=True)
#     #         dict_l = i.to_dict('list')
#     #         dict_ls.append(dict_l)
#     #     for i in dict_ls:
#     #         for dct in i:
#     #             if dct == 'PATIENT NAME':
#     #                 i[dct] = list(filter(None, i[dct]))
#     #                 result_kvp.append({'label': 'Patient_Name', 'extraction': i[dct][0]})
#     #             # if dct == 'Provider NPI':
#     #             #     i[dct] = list(filter(None, i[dct]))
#     #             #     result_kvp.append({'label': 'Claim_Id', 'extraction': i[dct][0]})
#     #     for i in dict_ls:
#     #         if isinstance(i, dict):
#     #             new_dct = {}
#     #             for dct in i:
#     #                 i[dct] = list(filter(None, i[dct]))
#     #                 for key in result_kvp:
#     #                     if key['extraction'] == i['PATIENT NAME'][0]:
#     #                         new_dct[key['extraction']] = i
#     #             if new_dct:
#     #                 patient_list.append(new_dct)
#     #     # patient_name = []
#     #     for patient in patient_list:
#     #         for pat_name in patient:
#     #             patient_name.append(pat_name)
#     #     # for d in result_kvp:
#     #     #     if d['label'] == 'Patient_Name':
#     #     #         result_kvp.remove(d)
#     #     result_kvp_new = []
#     #     for patient in patient_list:
#     #         result_kv_dict = {}
#     #         for pat_name in patient:
#     #             value_dt = [
#     #                 # {'label': 'Claim_Id', 'extraction': patient[pat_name]['Provider NPI'][0] or 'None'},
#     #                 {'label': 'Patient_Name', 'extraction': pat_name or None},
#     #                 {'label': 'Policy_Certificate', 'extraction': patient[pat_name]['Policy/Certificate'][0] or None},
#     #                 {'label': 'Patient_Acct_No', 'extraction': patient[pat_name]['Patient Account'][0] or None},
#     #                 {'label': 'Claim_Total', 'extraction': patient[pat_name]['Claim Total'][0] or None},
#     #                 result_kvp
#     #             ]
#     #
#     #             result_kv_dict[pat_name] = value_dt
#     #
#     #         result_kvp_new.append(result_kv_dict)
#     #
#     #     final_list_kvp = []
#     #     for dt in result_kvp_new:
#     #         for dctt in dt:
#     #             final_list_kvp.append(dt[dctt])
#     #
#     #     if len(final_list_kvp) == 1:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #     elif len(final_list_kvp) == 2:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#     #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#     #         form_text_2 = list(final_list_kvp[1])
#     #     elif len(final_list_kvp) == 3:
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace('[', '')
#     #         final_list_kvp[0] = str(final_list_kvp[0]).replace(']', '')
#     #         final_list_kvp[0] = ast.literal_eval(final_list_kvp[0])
#     #         form_text_1 = list(final_list_kvp[0])
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace('[', '')
#     #         final_list_kvp[1] = str(final_list_kvp[1]).replace(']', '')
#     #         final_list_kvp[1] = ast.literal_eval(final_list_kvp[1])
#     #         form_text_2 = list(final_list_kvp[1])
#     #         final_list_kvp[2] = str(final_list_kvp[2]).replace('[', '')
#     #         final_list_kvp[2] = str(final_list_kvp[2]).replace(']', '')
#     #         final_list_kvp[2] = ast.literal_eval(final_list_kvp[2])
#     #         form_text_3 = list(final_list_kvp[2])
#     #     else:
#     #         result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#     #         form_text_1 = result_kvp_1[0]
#     #         form_text_2 = result_kvp_1[1]
#     #         form_text_3 = result_kvp_1[2]
#     #
#     # final_df_list = []
#     # df = pd.DataFrame()
#     # for i in patient_list:
#     #     for d in i:
#     #         df = pd.DataFrame.from_dict(i[d], orient='index')
#     #         df = df.transpose()
#     #         final_df_list.append(df)
#     #
#     # result_df_list = []
#     # for f_df in final_df_list:
#     #     new_col = pd.DataFrame()
#     #     new_col['Patient_Name'] = f_df['PATIENT NAME']
#     #     new_col['SERV_DATE_FROM'] = f_df['DATE OF SERVICE']
#     #     new_col['SERV_DATE_TO'] = f_df['DATE OF SERVICE']
#     #     new_col['PROC'] = f_df['PROCEDURE CODE']
#     #     new_col['MODS'] = f_df['MEDICARE APPROVED']
#     #     new_col['BILLED/Charged'] = f_df['AMOUNT PAID']
#     #     new_col['DEDUCTIBLE'] = f_df['DEDUCTIBLE AMOUNT']
#     #     new_col['COPAY'] = f_df['CO-PAY AMOUNT']
#     #     new_col['Denial_Code/Explanation Codes/Adjustment_Reason'] = pd.Series([None])
#     #     new_col['GRP_RC_AMT'] = pd.Series([None])
#     #     new_col['PROV_PD'] = pd.Series([None])
#     #     new_col['Patient_responsibility'] = pd.Series([None])
#     #     new_col['ALLOWED'] = pd.Series([None])
#     #     new_col['No Of Unit/Quantity'] = pd.Series([None])
#     #     result_df_list.append(new_col)
#     #
#     # for d_f in result_df_list:
#     #     str_io = io.StringIO()
#     #     d_f.to_html(buf=str_io, classes='table table-striped')
#     #     html_str = str_io.getvalue()
#     #     html_df.append(html_str)
#     # html_df = list(set(html_df))
#         result_kvp = sorted(result_kvp, key=lambda r: r['label'])
#         result_kvp_1 = split_list(result_kvp, wanted_parts=3)
#         form_text_1 = result_kvp_1[0]
#         form_text_2 = result_kvp_1[1]
#         form_text_3 = result_kvp_1[2]
#
#         if form_text_1:
#             form_text_1 = clean_dict(form_text_1)
#             form_text_1 = [dict(t) for t in {tuple(d.items()) for d in form_text_1}]
#         if form_text_2:
#             form_text_2 = clean_dict(form_text_2)
#             form_text_2 = [dict(t) for t in {tuple(d.items()) for d in form_text_2}]
#         if form_text_3:
#             form_text_3 = clean_dict(form_text_3)
#             form_text_3 = [dict(t) for t in {tuple(d.items()) for d in form_text_3}]
#
#         dir_name = path + "/"
#         test = os.listdir(dir_name)
#         for item in test:
#             if item.endswith(".pdf"):
#                 os.remove(os.path.join(dir_name, item))
#         try:
#             if os.path.exists(path + '/output.csv'):
#                 # if page_format == 'Aetna_2':
#                 #     table_json, list_of_new_dfs = aetna_2_table_df(path + '/output.csv')
#                 # elif page_format == 'Aetna_3':
#                 #     table_json, list_of_new_dfs = aetna_3_table_df(path + '/output.csv', pat_info_table_index)
#                 # elif page_format == 'Aetna_1':
#                 #     table_json, list_of_new_dfs = aetna_1_table_df(path + '/output.csv')
#                 # elif page_format == 'Aetna_4':
#                 #     table_json, list_of_new_dfs = aetna_4_table_df(path + '/output.csv')
#                 # elif page_format == 'CIGNA_3':
#                 #     table_json, list_of_new_dfs = cigna_3_table_df(path + '/output.csv')
#                 # elif page_format == 'CIGNA_2':
#                 #     table_json, list_of_new_dfs = cigna_2_table_df(path + '/output.csv')
#                 # elif page_format == 'CIGNA_4':
#                 #     table_json, list_of_new_dfs = cigna_4_table_df(path + '/output.csv')
#                 # elif page_format == 'UHC_1':
#                 #     table_json, list_of_new_dfs = uhc_1_table_df(path + '/output.csv')
#                 # elif page_format == 'UHC_5':
#                 #     table_json, list_of_new_dfs = uhc_5_table_df(path + '/output.csv')
#                 # else:
#                 table_json, list_of_new_dfs = common_format_table_df(path + '/output.csv')
#                 df = pd.read_csv(path + '/output.csv')
#                 table_json = df.to_json(orient="records")
#
#             KvpJsonObj = KvpJson(file_name=filename, client_name=client_name,
#                                  claim_data=json.dumps(result_kvp),
#                                  table_data=json.dumps(table_json))
#             KvpJsonObj.save()
#         except Exception as e:
#             print(e)
#         for d_f in list_of_new_dfs:
#             str_io = io.StringIO()
#             d_f.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
#             html_str = str_io.getvalue()
#             html_df.append(html_str)
#         # html_df = list(set(html_df))
#         # if page_format == 'Aetna_2':
#         #     sequence_table_info = my_zip_longest((patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df),
#         #                                          (patient_name_l_1[0], claim_id_l_1[0],
#         #                                           patient_acct_no_l_1[0], html_df[0]))
#         #     # sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1, html_df,
#         #     #                                   fillvalue=patient_name_l_1[0])
#         # elif page_format == 'Aetna_3':
#         #     sequence_table_info = zip_longest(patient_name_l_1, claim_id_l_1, patient_acct_no_l_1,
#         #                                       html_df, fillvalue="")
#         # else:
#         if len(html_df) == 1:
#             patient_name_l_1 = [p_n] or ['None']
#             claim_id_l_1 = [c_n] or ['None']
#             patient_acct_no_l_1 = [p_a_n] or ['None']
#         else:
#             patient_name_l_1 = patient_name_l_1[:len(html_df)]
#             claim_id_l_1 = claim_id_l_1[:len(html_df)]
#             patient_acct_no_l_1 = patient_acct_no_l_1[:len(html_df)]
#         sequence_table_info = zip_longest(patient_name_l_1,
#                                               claim_id_l_1, patient_acct_no_l_1,
#                                               html_df, fillvalue="")
#         table_dfs_json = []
#         if len(list_of_new_dfs) != 0:
#             l_df = pd.concat(list_of_new_dfs)
#             table_dfs_json = l_df.to_dict("records")
#         new_s_list = []
#         new_s_list.append(patient_name_l_1)
#         new_s_list.append(claim_id_l_1)
#         new_s_list.append(patient_acct_no_l_1)
#         csv_format = csv_formation(result_kvp, table_dfs_json, new_s_list)
#         return render(request, 'home.html', {
#             "csv_file": csv_format,
#             "form_text_1": form_text_1,
#             "form_text_2": form_text_2,
#             "form_text_3": form_text_3,
#             "file_name": filename,
#             "dfs": sequence_table_info
#         })
#         # except Exception as e:
#         #     print(e)
#     return render(request, 'home.html')


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts] for i in range(wanted_parts)]


def my_zip_longest(iterables, fillvalues):
    max_len = max(len(i) for i in iterables)
    return zip(*[list(i) + [v] * (max_len - len(i)) for i, v in zip(iterables, fillvalues)])


def csv_formation(result_kvp, table_dfs_json, new_s_list):
    df3 = pd.DataFrame(new_s_list)
    df3 = df3.transpose()
    df3.columns = ['Patient_Name', 'Claim_ID', 'Patient_Acc_No']
    df2 = pd.DataFrame(table_dfs_json)
    service_line_df = pd.concat([df3, df2], axis=0)
    service_line_df = service_line_df.apply(lambda x: pd.Series(x.dropna().values))

    new_result_kvp = []
    for i in result_kvp:
        new_dict = {}
        new_dict[i['label']] = [i['extraction']]
        new_result_kvp.append(new_dict)
    res = {}
    for dict in new_result_kvp:
        for list in dict:
            if list in res:
                res[list] += (dict[list])
            else:
                res[list] = dict[list]
    df5 = pd.DataFrame.from_dict(res, orient='index')

    output_file = path + '/csv_new_format.xlsx'
    with pd.ExcelWriter(output_file) as writer:
        df5.T.to_excel(writer, sheet_name='Claim_info')
        service_line_df.to_excel(writer, sheet_name='Service_line_info')

    return output_file


#  With Azure API REsult-


import json
import time
import getopt
import sys
import os
from requests import get, post
import csv


def home(request):
    """
    :param request:
    :return:
    """
    # csv_file = 'output.csv'
    if request.method == 'POST' and request.FILES['files']:
        selected = request.POST['model_name']
        myfile_list = request.FILES.getlist('files')
        folder_result = {}
        dir_name = path + '/multiple_files/'+selected+'/'
        test = os.listdir(dir_name)
        for item in test:
            if item.endswith(".csv"):
                os.remove(os.path.join(dir_name, item))
        for myfile in myfile_list:
            claim_df = []
            table_df = []
            all_df = []
            claim_html_df = []
            table_html_df = []
            if selected == "BCBS":
                # model_name = "50a16c78-d2c9-47b2-b5fd-c78292db8fde"
                # Update on March 14, 2022
                # model_name = "6e7b494c-a93a-4263-a91b-11915e90b01f"
                #  Update on March 21, 2022
                model_name = "e3ff92f3-c4b5-4d6e-acaa-e615b3c2af28"
                folder_path = path + '/multiple_files/BCBS/'
            elif selected == "Cigna":
                # Added on 8 March 2022
                # model_name = "71050399-fa8f-4137-8d89-b52d4c03aa7b"
                # Added on 17 March 2022
                model_name = "55e5445e-6998-4159-8655-6e05157f344a"
                folder_path = path + '/multiple_files/Cigna/'
            elif selected == "AETNA":
                # model_name = "407e7816-e4a4-403a-8de8-8fedbd79bbfb"
                # model_name = "83998ab2-8faf-4610-b891-595593c6b349"
                # Added on 7 march 2022
                model_name = "e456e498-a072-4e91-8372-f6f691594026"
                folder_path = path + '/multiple_files/AETNA/'
            elif selected == "HUMANA":
                # model_name = "de66c39e-63f9-4052-9bfd-712cc3cf6fa8"
                # Added on 7 march 2022
                model_name = "06431946-d015-4da9-ba26-e1ab0228548f"
                folder_path = path + '/multiple_files/HUMANA/'
            else:
                model_name = "8a6e5313-3711-4ef2-bcdf-a260cbeef6cd"
                folder_path = path + '/multiple_files/'

            fs = FileSystemStorage(location=folder_path)
            filename = fs.save(myfile.name, myfile)
            sp = filename.split('.')[0]
            files = folder_path + filename
            file_type = filetype.guess(files)
            # image_or_file = ''
            pat_info_list = []
            if file_type.extension == 'pdf':
                pages3 = convert_from_bytes(open(files, 'rb').read())
                for idx, page in enumerate(pages3):
                    # img = enhance_image(page)
                    # cv2.imwrite(path + '/enhanced.jpg', img)
                    page.save(path + '/enhanced.jpg', 'jpeg')
                    documentName = path + '/enhanced.jpg'

                    try:
                        json_resp = Azure_runAnalysis(documentName, model_name)
                    except Exception as e:
                        print(e)
                    try:
                        # start3 = time.time()
                        c_df = claim_rep(json.loads(json_resp))
                        if not c_df.empty:
                            claim_df.append(c_df)
                            all_df.append(c_df)
                        t_df = table_rep(json.loads(json_resp))
                        if not t_df.empty:
                            table_df.append(t_df)
                            all_df.append(t_df)
                        # end3 = time.time()
                    except Exception as e:
                        print(e)

            dir_name = folder_path
            test = os.listdir(dir_name)
            for item in test:
                if item.endswith(".pdf"):
                    os.remove(os.path.join(dir_name, item))

            csv_folder_path = folder_path
            with open(csv_folder_path+sp+'_.csv', 'a') as f:
                for df in all_df:
                    df = df.applymap('="{}"'.format)
                    df.to_csv(f)
                    f.write("\n\n")

            # Claim_DF to HTML
            for d_f in claim_df:
                str_io = io.StringIO()
                d_f.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
                claim_df_html_str = str_io.getvalue()
                claim_html_df.append(claim_df_html_str)
            # str_io = io.StringIO()
            # claim_df.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
            # claim_df_html_str = str_io.getvalue()

            # Table_DF to html
            for d_f in table_df:
                str_io = io.StringIO()
                d_f.to_html(buf=str_io, classes='table table-striped projectSpreadsheet table_1')
                table_df_html_str = str_io.getvalue()
                table_html_df.append(table_df_html_str)

        dir_csv_files = []
        test = os.listdir(folder_path)

        return render(request, 'home.html', {
            # 'claim_df_html_str': claim_html_df,
            # 'table_df_html_str': table_html_df,
            "folder": selected,
            "file_name": test
        })
    return render(request, 'home.html')


def Azure_runAnalysis(input_file, model_name):
    # Endpoint URL
    endpoint = r"https://eastus.api.cognitive.microsoft.com/"
    # Subscription Key
    apim_key = "be530c41e9b54116a7944d48ac6a9b08"
    # Model ID
    # model_id = "61d83680-b665-41b4-bcad-28c49e3672c1"
    # New_Model_14_Dec_Update for all format
    # model_id = "6680e22e-4cb7-4751-8708-3694571446b8"
    # Updated_Model_23_Dec.
    # model_id = "8a6e5313-3711-4ef2-bcdf-a260cbeef6cd"
    model_id = model_name
    # All Mix_ OLD EOB and AETNA humana, etc by 25_dec.

    # model_id = 'd2a45385-2a51-467b-9db1-a8c563dcaeb3'
    # API version
    API_version = "v2.1"

    post_url = endpoint + "/formrecognizer/%s/custom/models/%s/analyze" % (API_version, model_id)
    params = {
        "includeTextDetails": True,
        "locale": "en-US"
    }

    headers = {
        # Request headers
        'Content-Type': 'application/pdf',
        'Ocp-Apim-Subscription-Key': apim_key,
    }
    try:
        with open(input_file, "rb") as f:
            data_bytes = f.read()
    except IOError:
        sys.exit(2)

    try:
        resp = post(url = post_url, data = data_bytes, headers = headers, params = params)
        if resp.status_code != 202:
            quit()
        get_url = resp.headers["operation-location"]
    except Exception as e:
        print("POST analyze failed:\n%s" % str(e))
        quit()

    n_tries = 15
    n_try = 0
    wait_sec = 5
    max_wait_sec = 60
    while n_try < n_tries:
        try:
            resp = get(url=get_url, headers={"Ocp-Apim-Subscription-Key": apim_key})
            resp_json = resp.json()
            if resp.status_code != 200:
                return json.dumps(resp_json)
                # quit()
            status = resp_json["status"]
            if status == "succeeded":
                return json.dumps(resp_json, indent=2, sort_keys=True)
                # quit()
            if status == "failed":
                return json.dumps(resp_json)
                # quit()
            # Analysis still running. Wait and retry.
            time.sleep(wait_sec)
            n_try += 1
            wait_sec = min(2*wait_sec, max_wait_sec)
        except Exception as e:
            msg = "GET analyze results failed:\n%s" % str(e)
            print(msg)
            # quit()


def claim_rep(json):
    claim_list = []
    for i in json['analyzeResult']['documentResults']:
        claim_dict = {}
        for j in i['fields']:
            if j != 'EOB_table' or j != 'EOD_table' or j != 'EOB table':
                val = str(i['fields'][j].get('text')).replace('|', '')
                claim_dict[j] = val

        if len(claim_dict.keys()) > 1:
            if len(list(set(list(claim_dict.values())))) != 1:
                claim_list.append(claim_dict)

    claim_df = pd.DataFrame(claim_list)
    if claim_df.isnull().sum(axis=1)[0] >= 11:
        return pd.DataFrame()
    return claim_df


def table_rep(json):
    table_list = []
    for i in json['analyzeResult']['documentResults']:
        for j in i['fields']:
            if j == 'EOB_table' or j == 'EOD_table' or j == 'EOB table':
                for t in i['fields'][j]['valueArray']:
                    table_dict = {}
                    for k in t['valueObject']:
                        val = str(t['valueObject'][k].get('text')).replace('|A', '')
                        val = val.replace('|', '')
                        val = val.replace('}', '')
                        val = val.replace('£', '')
                        val = val.replace(';', '')
                        val = val.replace('[', '')
                        val = val.replace(':', '')
                        val = val.replace('...', '.')
                        val = val.replace('..', '.')
                        val = val.replace('DiMember :', '.')
                        val = val.replace('.**:-', '.')
                        val = val.replace(')', '')
                        val = val.replace('(', '')
                        val = val.replace('#', '')
                        table_dict[k] = val
                    table_list.append(table_dict)
    table_df = pd.DataFrame(table_list)
    if table_df.isnull().sum(axis=1)[0] >= 11:
        return pd.DataFrame()
    else:
        return table_df
