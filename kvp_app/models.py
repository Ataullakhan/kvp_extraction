from django.db import models
# Create your models here.
from django.db.models import JSONField


class KvpJson(models.Model):
    file_name = models.CharField(max_length=200, blank=True, null=True)
    client_name = models.CharField(max_length=50, blank=True, null=True)
    claim_data = JSONField(blank=True, null=True)
    table_data = JSONField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        db_table = 'kvp_json'