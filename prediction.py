from kvp_extraction import settings
import os
import re
import numpy as np
import cv2
import argparse
path = settings.MEDIA_ROOT
org_img = path+'/converted.jpg'
ll_labels = ['Allowable_Amt','Applied_Copay','Check_Amount','Check_No','Co_Insurance','Date_Service','Deductible',
         'Group_No','Member','Not_Payable','Patient_Acct_No','Patient_Address','Patient_Mem_ID','Patient_Name',
         'Payable_Amt','Pro_Rev_Code','Processed_Date','Sub_Amt_Bill_Charges','Table','Total_Charges']

model = path + '/models/custom-yolov4-detector_best.weights'

def text_extract(org_img):
    # os.system("chmod a+x media/darknet/ && darknet detect cfg/custom-yolov4-detector.cfg {0} {1} -dont-show -ext_output < {2} > result.txt".format(
    #         model, org_img, org_img))
    # os.system(
    #     '{0}/darknet/darknet detect cfg/custom-yolov4-detector.cfg {1} {2} -dont-show -ext_output < {3} > result.txt'.format(
    #         path, model, org_img, org_img))
    parser = argparse.ArgumentParser()
    parser.add_argument('--image', default=org_img, help="image for prediction")
    parser.add_argument('--config', default=path+'/darknet/cfg/custom-yolov4-detector.cfg', help="YOLO config path")
    parser.add_argument('--weights', default=model, help="YOLO weights path")
    parser.add_argument('--names', default=path+'/darknet/data/coco.names', help="class names path")
    args = parser.parse_args()

    CONF_THRESH, NMS_THRESH = 0.5, 0.5

    # Load the network
    net = cv2.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

    # Get the output layer from YOLO
    layers = net.getLayerNames()
    output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # Read and convert the image to blob and perform forward pass to get the bounding boxes with their confidence scores
    img = cv2.imread(args.image)
    height, width = img.shape[:2]

    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)
    layer_outputs = net.forward(output_layers)

    class_ids, confidences, b_boxes = [], [], []
    for output in layer_outputs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > CONF_THRESH:
                center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                b_boxes.append([x, y, int(w), int(h)])
                confidences.append(float(confidence))
                class_ids.append(int(class_id))

    print(class_ids, confidences, b_boxes)
    return class_ids, confidences, b_boxes

text_extract(org_img)